# bkstat

`bkstat` provides functions for statistical modeling.

## Installation

devtools::install_bitbucket("bklamer/bkstat")
