#' Normalize a numeric vector
#'
#' Normalize a numeric vector. `normalize()` defaults to `[0, 1]` normalization.
#' `normalize_maxabs()` returns maximum absolute normalization.
#'
#' @param x A numeric vector.
#' @param lower A numeric for the lower bound.
#' @param upper A numeric for the upper bound.
#'
#' @return numeric vector
#'
#' @importFrom bkmisc check_numeric
#'
#' @export
#'
#' @examples
#' #----------------------------------------------------------------------------
#' # normalize() examples
#' #----------------------------------------------------------------------------
#' library(bkstat)
#'
#' x <- rnorm(100, mean = 30, sd = 50)
#' normalize(x)
#' normalize_maxabs(x)
#'
normalize <- function(x, lower = 0, upper = 1) {
  #-----------------------------------------------------------------------------
  # Check arguments
  #-----------------------------------------------------------------------------
  check_numeric(x)
  check_numeric(lower)
  check_numeric(upper)
  stopifnot(length(lower) == 1)
  stopifnot(length(upper) == 1)

  #-----------------------------------------------------------------------------
  # Return
  #-----------------------------------------------------------------------------
  ((x - min(x)) * (upper - lower)) / (max(x) - min(x)) + lower
}

#' @rdname normalize
#' @export
normalize_maxabs <- function(x) {
  #-----------------------------------------------------------------------------
  # Check arguments
  #-----------------------------------------------------------------------------
  check_numeric(x)

  #-----------------------------------------------------------------------------
  # Return
  #-----------------------------------------------------------------------------
  x / max(abs(x))
}
