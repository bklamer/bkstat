#' Marginal variance-covariance matrix from an LMM
#'
#' Calculates the marginal variance-covariance matrix from an `lme4::lmer` or
#' `nlme::lme` object.
#'
#' For linear mixed model \eqn{\boldsymbol{y}_i = \boldsymbol{X}_i \boldsymbol{\beta} +
#' \boldsymbol{Z}_i \boldsymbol{b}_i + \boldsymbol{\epsilon}_i},
#' where \eqn{i} is the group index, \eqn{\boldsymbol{y}_i} is an \eqn{n_i}-dimensional
#' vector of observed responses, \eqn{\boldsymbol{X}_i} and \eqn{\boldsymbol{Z}_i}
#' are known \eqn{n_i \times p} and \eqn{n_i \times q} regression matrices corresponding
#' to the \eqn{p}-dimensional fixed effects vector \eqn{\boldsymbol{\beta}} and the
#' \eqn{q}-dimensional random effects vector \eqn{b_i}, respectively, and \eqn{\boldsymbol{\epsilon}_i}
#' is an \eqn{n_i}-dimensional vector of within-group errors.
#'
#' The \eqn{\boldsymbol{b}_i} are assumed to be independently distributed as
#' \eqn{N(\boldsymbol{0}, \boldsymbol{\Psi})} and the \eqn{\boldsymbol{\epsilon}_i}
#' are assumed to be independently distributed as
#' \eqn{N(\boldsymbol{0}, \boldsymbol{\Lambda}_i)}, independent of the
#' \eqn{\boldsymbol{b}_i}. The \eqn{\boldsymbol{\Psi}} covariance matrix may be
#' unstructured or structured (commonly a compound symmetric or general symmetric
#' positive definite matrix parameterized by \eqn{\boldsymbol{\theta}}).
#' The \eqn{\boldsymbol{\Lambda}} matrices are assumed to depend on \eqn{i} only through
#' their dimensions \eqn{\boldsymbol{\lambda}} (commonly a diagonal, compound/general
#' symmetric, or AR(1) matrix parameterized by \eqn{\boldsymbol{\lambda}}).
#'
#' We are interested in jointly estimating the regression model parameters
#' (relationship between response and covariates) and the covariance parameters
#' (the correlation and variance structure of the response). The model approach using
#' random effects is called the conditional model. The model approach which directly
#' models the within-group error covariance is the called the marginal model.
#'
#' Likelihood estimation is based on the marginal distribution of the observed
#' response vectors \eqn{\boldsymbol{y}_i}. Since the random effects and within-group
#' error term enter the model linearly, it can be shown that the \eqn{\boldsymbol{y}_i}
#' are marginally distributed as independent
#' \eqn{N(\boldsymbol{X}_i\boldsymbol{\beta}, \boldsymbol{\Sigma}_i)} random vectors.
#' The marginal covariance matrix is given by \eqn{var(\boldsymbol{y}_i) =
#' \boldsymbol{\Sigma}_i = \boldsymbol{Z}_i \boldsymbol{\Psi} \boldsymbol{Z}_i^{\prime} +
#' \boldsymbol{\Lambda}_i}, where \eqn{\boldsymbol{Z}_i \boldsymbol{\Psi} \boldsymbol{Z}_i^{\prime}}
#' is the random effects variance component and \eqn{\boldsymbol{\Lambda}_i} is
#' the within-group error variance component.
#'
#' Linear mixed effects models can account for within-group correlation and heteroscedasticity
#' through the random effects \eqn{\boldsymbol{b}_i} and the within-group errors \eqn{\boldsymbol{\epsilon}_i}.
#' Since the \eqn{\boldsymbol{b}_i} are fixed by group, the within-group observations
#' share the same random effects and are correlated. The \eqn{\boldsymbol{Z}_i \boldsymbol{\Psi} \boldsymbol{Z}_i^{\prime}}
#' variance component describes this correlation. The diagonal of \eqn{\boldsymbol{Z}_i \boldsymbol{\Psi} \boldsymbol{Z}_i^{\prime}}
#' can also accommodate heteroscedasticity. If fitting a conditional model, \eqn{\boldsymbol{Z}_i \boldsymbol{\Psi} \boldsymbol{Z}_i^{\prime}}
#' is usually favored for describing the structure of the data and \eqn{\boldsymbol{\Lambda}_i} typically
#' assumes a simple form such as \eqn{\boldsymbol{\Lambda}_i = \sigma^2 \boldsymbol{I}_i}.
#' If fitting a marginal model, \eqn{\boldsymbol{\Sigma}_i = \boldsymbol{\Lambda}_i}
#' so it must accommodate correlation (non-diagonal elements) and heteroscedasticity.
#'
#' In practice of fitting LMMs, one can use either variance component to account
#' for correlation and heteroscedasticity. However, the two components may compete
#' in explaining the marginal covariance and lead to overparameterized models.
#'
#' @param x An `lme4::lmer` or `nlme::lme` model object.
#'
#' @return dgCMatrix
#'
#' @seealso \code{nlme::\link[nlme]{getVarCov}}
#'
#' @importFrom lme4 getME
#' @importFrom stats sigma
#' @importFrom Matrix crossprod t Diagonal bdiag
#' @importFrom nlme getVarCov
#'
#' @references https://stackoverflow.com/a/45655597
#'
#' \insertRef{pinheiro_2006}{bkstat}
#'
#' \insertRef{venables_2002}{bkstat}
#'
#' \insertRef{pinheiro_2000}{bkstat}
#'
#' @export
#'
#' @examples
#'
#' #----------------------------------------------------------------------------
#' # lmm_marginal_varcov() examples
#' #----------------------------------------------------------------------------
#' library(bkstat)
#'
#' data(Orthodont, package="nlme")
#'
#' mod_lmer <- lme4::lmer(
#'   formula = distance ~ age + Sex + (1 + age | Subject),
#'   data = Orthodont
#' )
#' mod_lme <- nlme::lme(
#'   fixed = distance ~ age + Sex,
#'   random = ~ 1 + age | Subject,
#'   #correlation = nlme::corCompSymm(form = ~ 1 | Subject),
#'   data = Orthodont
#' )
#'
#' vcov_lmer <- lmm_marginal_varcov(mod_lmer)
#' vcov_lme <- lmm_marginal_varcov(mod_lme)
#' vcov_lme_check <- nlme::getVarCov(
#'   obj = mod_lme,
#'   individuals = levels(mod_lme$groups$Subject),
#'   type = "marginal"
#' )
#' vcov_lme_check <- Matrix::bdiag(vcov_lme_check)
#'
#' Matrix::image(vcov_lmer)
#' Matrix::image(vcov_lme)
#' Matrix::image(vcov_lme_check)
#'
#' vcov_lmer[1:8, 1:8]
#' vcov_lme[1:8, 1:8]
#' vcov_lme_check[1:8, 1:8]
#'
lmm_marginal_varcov <- function(x) {
  if(inherits(x, "lmerMod")) {
    # extract components
    var_b <- Matrix::crossprod(x = lme4::getME(object = x, name = "Lambdat"))
    Zt <- lme4::getME(x, "Zt")
    var_error <- sigma(x)^2

    # combine them
    var_re <- var_error * (Matrix::t(Zt) %*% var_b %*% Zt)
    var_error_I <- var_error * Matrix::Diagonal(nrow(x@frame))
    var_y <- var_re + var_error_I

    # return
    var_y
  } else if(inherits(x, "lme")) {
    var_y <- getVarCov(
      obj = x,
      individuals = levels(x$groups[[1]]), # TODO When does this fail?
      type = "marginal"
    )

    # return
    Matrix::bdiag(var_y)
  } else {
    stop("Can't calculate the covariance matrix with 'x' argument of class: ", bkmisc::pretty_print(class(x)))
  }
}
