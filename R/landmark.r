#' Create a landmark dataset
#'
#' @description
#' This function will generate
#' - Summary statistics about the conversion to landmark data.
#' - A dataframe that can be used for landmark survival analysis
#'
#' It can't handle longitudinal survival data or multiple variables that need landmark adjustment.
#'
#' @details
#' Landmark analysis is used to correct for survival bias (immortal time bias). This
#' bias may occur when the survival model includes a covariate which was measured
#' after the starting point of survival time (after baseline survival time). This
#' is common for treatment indicator variables. If patients must survive until
#' they had a chance to receive treatment, then they will by definition have longer
#' survival times under naive analysis of treatment status!
#'
#' Landmark analysis is the simplest way to control for this bias. It provides
#' unbiased survival probabilities conditional on the landmark covariate membership
#' at the landmark time. Using the landmark method, survival analysis is performed
#' as usual and the final interpretation is conditioned as "Among patients who are
#' still at risk and measured for the landmark covariate at landmark time, we find..."
#'
#' An alternative analysis method to landmark analysis is through use of a time-dependent
#' Cox model. This model is more complicated to use, but is a useful alternative when
#'
#' - There is no agreed upon landmark time.
#' - You have longitudinal data (covariate values change over time)
#' - There are too many patients excluded at the chosen landmark time. Generally you do not want the landmark covariate to be measured at extended points of time past baseline.
#'
#' @param df A data frame.
#' @param landmark Numeric for the landmark time. Should be in same units as the survival time and landmark covariate time.
#' @param var String for name of variable that requires landmark adjustment, i.e. this variable may have measurements after baseline survival time.
#' @param var_baseline String for name of baseline level of landmark variable. i.e. what should the default value be set to when measured after landmark time.
#' @param var_time Numeric for the time of measurement for landmark variable. Should be in same units as the survival time.
#' @param time String for name of survival time variable.
#' @param status String for name of survival status variable.
#' @param status_event String for status level name indicating event occurrence. i.e. could be "1", "Death", "TRUE", etc.
#'
#' @references
#' \insertRef{dafni_2011}{bkstat}
#'
#' @return A list containing two items:
#' - `summary`: a data.frame of landmark conversion summaries
#' - `data`: a data.frame for landmark analysis
#'
#' @importFrom bkmisc check_data_frame check_string check_numeric
#'                    check_name_in_df check_x_in_y
#'
#' @export
#'
#' @examples
#' #----------------------------------------------------------------------------
#' # landmark() examples.
#' #----------------------------------------------------------------------------
#' library(survival)
#' library(bkstat)
#'
#' # simple survival dataset
#' df <- data.frame(
#'   time = c(4,3,1,3,2.5,1,3),
#'   status = c(1,1,1,0,1,1,0),
#'   x = c(0,2,1,1,1,0,0),
#'   x_time = c(1,3,1,1,1,2,1),
#'   sex = c(1,0,0,0,1,1,1)
#' )
#'
#' coxph(Surv(time, status) ~ x + sex, df)
#'
#' df2 <- landmark(
#'   df = df,
#'   landmark = 2,
#'   var = "x",
#'   var_baseline = "0",
#'   var_time = "x_time",
#'   time = "time",
#'   status = "status",
#'   status_event = "1"
#' )
#' df2$summary
#' df2$data
#'
#' coxph(Surv(time, status) ~ x + sex, df2$data)
#'
landmark <- function(
  df,
  landmark,
  var,
  var_baseline,
  var_time,
  time,
  status,
  status_event
) {
  #-----------------------------------------------------------------------------
  # Check arguments
  #-----------------------------------------------------------------------------
  check_data_frame(df)
  check_numeric(landmark)

  check_string(var)
  check_name_in_df(name = var, df = df)
  check_string(var_baseline)
  check_x_in_y(x = var_baseline, y = df[[var]])
  check_string(var_time)
  check_name_in_df(name = var_time, df = df)

  check_string(time)
  check_name_in_df(name = time, df = df)
  check_string(status)
  check_name_in_df(name = status, df = df)
  check_string(status_event)
  check_x_in_y(x = status_event, y = df[[status]])

  n_orig <- nrow(df)

  #-----------------------------------------------------------------------------
  # Transform data
  # Important to save some metadata about the landmark process so that
  # it can be reported later. i.e. save exclusions and data changes
  #-----------------------------------------------------------------------------
  # we'll update these, if needed, below
  n_is_status_na <- 0
  n_is_time_na <- 0
  n_is_time_before_landmark <- 0
  n_censor_before_landmark <- 0
  n_event_before_landmark <- 0
  n_is_var_time_na <- 0
  n_is_var_time_after_landmark <- 0
  n_is_var_na <- 0

  # Remove rows:
  #     - with missing survival status
  #     - with missing survival time
  #     - that had survival time before landmark time
  is_status_na <- is.na(df[[status]])
  if(any(is_status_na)) {
    n_is_status_na <- sum(is_status_na)
    df <- df[!is_status_na, ]
  }

  is_time_na <- is.na(df[[time]])
  if(any(is_time_na)) {
    n_is_time_na <- sum(is_time_na)
    df <- df[!is_time_na, ]
  }

  is_time_before_landmark <- df[[time]] <= landmark
  if(any(is_time_before_landmark)) {
    n_is_time_before_landmark <- sum(is_time_before_landmark)
    n_censor_before_landmark <- sum(is_time_before_landmark & (df[[status]] != status_event))
    n_event_before_landmark <- sum(is_time_before_landmark & (df[[status]] == status_event))
    df <- df[!is_time_before_landmark, ]
  }

  # Remove rows with:
  #     - missing landmark covariate time
  #     - missing landmark covariate values
  # and, set variable level, whose time is after landmark, to baseline level.
  #
  # We can fill in missing variable levels if the time is known to occur after
  # landmark.
  #
  # Since baseline level can be the lack of something occurring, it may not
  # have a time associated with it, i.e. recorded as NA. So ignore baseline
  # level when checking for NA times.
  is_var_time_na <- is.na(df[[var_time]]) & (df[[var]] != var_baseline) & !is.na(df[[var]])
  if(any(is_var_time_na)) {
    n_is_var_time_na <- sum(is_var_time_na)
    df <- df[!is_var_time_na, ]
  }

  is_var_time_after_landmark <- (df[[var_time]] > landmark) & !is.na(df[[var_time]])
  if(any(is_var_time_after_landmark)) {
    n_is_var_time_after_landmark <- sum(is_var_time_after_landmark)
    # table() will ignore missing values, thus not counted in these summary stats
    var_levels_numerator <- as.data.frame(table(df[[var]], is_var_time_after_landmark), stringsAsFactors = FALSE)
    var_levels_numerator <- var_levels_numerator[var_levels_numerator$is_var_time_after_landmark == "TRUE" & var_levels_numerator$Var1 != var_baseline, ]
    var_levels_denominator <- as.data.frame(table(df[[var]]), stringsAsFactors = FALSE)
    var_levels_denominator <- var_levels_denominator[var_levels_denominator$Var1 != var_baseline, ]
    df[is_var_time_after_landmark, var] <- var_baseline
  }

  # Now it's ok to remove missing variable values
  is_var_na <- is.na(df[[var]])
  if(any(is_var_na)) {
    n_is_var_na <- sum(is_var_na)
    df <- df[!is_var_na, ]
  }

  # Adjust survival time
  df[[time]] <- df[[time]] - landmark

  #-----------------------------------------------------------------------------
  # Prepare return object
  #-----------------------------------------------------------------------------
  n_landmark <- nrow(df)
  n_excluded <- n_orig - n_landmark
  n_baseline_var <- sum(df[[var]] == var_baseline)

  landmark_summary <- data.frame(
    summary = c(
      paste0("The chosen landmark time was ", landmark, "."),
      paste0("In total, ", n_excluded, " rows (", n_excluded, "/", n_orig, "=", round(100 * ((n_excluded) / n_orig), 0), "%) were excluded to create the landmark dataset (n=", n_landmark, ")."),
      paste0("First, ", n_is_status_na, " rows were excluded for missing values in survival status: '", status, "'."),
      paste0("Second, ", n_is_time_na, " rows were excluded for missing values in survival time: '", time, "'."),
      paste0("Third, ", n_is_time_before_landmark, " rows were excluded since their survival time, '", time, "', occured before the landmark."),
      paste0(n_censor_before_landmark, " rows (", n_censor_before_landmark, "/", n_is_time_before_landmark, "=", round(100 * (n_censor_before_landmark / n_is_time_before_landmark), 1), "%) were excluded for censoring before the landmark time."),
      paste0(n_event_before_landmark, " rows (", n_event_before_landmark, "/", n_is_time_before_landmark, "=", round(100 * (n_event_before_landmark / n_is_time_before_landmark), 1), "%) were excluded for event occurrence before the landmark time."),
      paste0("Fourth, ", n_is_var_time_na, " rows were excluded for missing values in landmark covariate time: '", var_time, "'."),
      paste0("Fifth, ", n_is_var_na, " rows were excluded for missing values in landmark covariate: '", var, "'."),
      paste0("Of the ", n_baseline_var, " rows that are now ", var, "=", var_baseline, ", ", n_is_var_time_after_landmark, " of them (", n_is_var_time_after_landmark, "/", n_baseline_var, "=", round(100 * ((n_is_var_time_after_landmark) / n_baseline_var), 0), "%) were converted to this value because they were recorded after landmark time."),
      if(any(is_var_time_after_landmark)) {
        paste0("Of the ", var_levels_denominator$Freq, " rows that were originally ", var, "=", var_levels_denominator$Var1, ", ", var_levels_numerator$Freq, " of them (", var_levels_numerator$Freq, "/", var_levels_denominator$Freq, "=", round(100 * (var_levels_numerator$Freq / var_levels_denominator$Freq), 1), "%) were converted to ", var, "=", var_baseline, ".")
      } else {
        NULL
      }
    ),
    value = c(
      landmark,
      n_excluded,
      n_is_status_na,
      n_is_time_na,
      n_is_time_before_landmark,
      n_censor_before_landmark,
      n_event_before_landmark,
      n_is_var_time_na,
      n_is_var_na,
      n_is_var_time_after_landmark,
      if(any(is_var_time_after_landmark)) {
        var_levels_numerator$Freq
      } else {
        NULL
      }
    )
  )

  ret <- list(
    summary = landmark_summary,
    data = df
  )

  #-----------------------------------------------------------------------------
  # Return
  #-----------------------------------------------------------------------------
  ret
}
