#' Convert `coxphw::coxphw` into a data.frame
#'
#' Converts a \code{coxphw::\link[coxphw]{coxphw}} object into a data.frame.
#'
#' @param x A \code{coxphw::\link[coxphw]{coxphw}} object.
#'
#' @return data.frame
#'
#' @seealso \code{coxphw::\link[coxphw]{coxphw}}
#'
#' @export as_df.coxphw
#' @usage as_df.coxphw(x)
#'
#' @examples
#' #----------------------------------------------------------------------------
#' # as_df.coxphw() examples.
#' #----------------------------------------------------------------------------
#' library(bkstat)
#' library(coxphw)
#'
#' data("gastric")
#'
#' # weighted estimation of average hazard ratio
#' fit1 <- coxphw(Surv(time, status) ~ radiation, data = gastric, template = "AHR")
#' summary(fit1)
#'
#' as_df.coxphw(fit1)
#'
as_df.coxphw <- function(x) {
  #-----------------------------------------------------------------------------
  # Check arguments
  #-----------------------------------------------------------------------------

  #-----------------------------------------------------------------------------
  # Format data.frame
  #-----------------------------------------------------------------------------
  se <- diag(x$var)^0.5
  out <- cbind(
    x$coefficients,
    se,
    exp(x$coefficients),
    x$ci.lower,
    x$ci.upper,
    x$coefficients/se,
    x$prob
  )
  dimnames(out) <- list(
    names(x$coefficients),
    c("coef", "se(coef)", "exp(coef)", paste(c("lower", "upper"), 1 - x$alpha), "z", "p")
  )
  if(!is.null(x$betafix)) {
    out[!is.na(x$betafix), -c(1, 3)] <- NA
  }
  out <- as.data.frame(out)
  out$p <- format.pval(out$p, digits = 2, eps = 0.001)
  out <- as.data.frame(cbind(predictor = rownames(out), data.frame(out, row.names = NULL)), row.names = NULL)

  #-----------------------------------------------------------------------------
  # Return
  #-----------------------------------------------------------------------------
  out
}
