#' Convert logits to probabilities
#'
#' Converts a vector of logits to probabilities.
#'
#' @param logit A numeric vector.
#'
#' @return numeric vector
#'
#' @importFrom stats plogis
#' @importFrom bkmisc check_numeric
#'
#' @export
#'
#' @examples
#' #----------------------------------------------------------------------------
#' # logit_inv() examples
#' #----------------------------------------------------------------------------
#' library(bkstat)
#'
#' logit <- c(-1, 0, 1)
#' logit_inv(logit)
#'
logit_inv <- function(logit){
  #-----------------------------------------------------------------------------
  # Check arguments
  #-----------------------------------------------------------------------------
  check_numeric(logit)

  #-----------------------------------------------------------------------------
  # Return
  # logistic(logit) = 1 / (1 + exp(-logit))
  # Or softmax-style equation (more numerically stable at extremes):
  # exp(logit) / (exp(logit) + 1)
  # https://github.com/johnmyleswhite/StatsFunctionsNotes/blob/master/A%20Numerically%20Accurate%20Inverse%20Logit%20Function%20for%2064-Bit%20Floating%20Point.ipynb
  #-----------------------------------------------------------------------------
  plogis(logit)
}
