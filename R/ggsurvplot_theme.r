#' Customize ggsurvplot() figures
#'
#' The simple method of adding/modifying themes or other parameters of
#' \code{survminer::\link[survminer]{ggsurvplot}} figures
#' doesn't seem to work after recent updates of ggplot2. This function is a workaround.
#'
#' @param p A ggsurvplot object.
#' @param font.title A vector with theme arguments for the title.
#' @param font.subtitle A vector with theme arguments for the subtitle.
#' @param font.caption A vector with theme arguments for the caption.
#' @param font.x A vector with theme arguments for the x axis font.
#' @param font.y A vector with theme arguments for the y axis font.
#' @param font.xtickslab A vector with theme arguments for the x ticks.
#' @param font.ytickslab A vector with theme arguments for the y ticks.
#'
#' @return ggsurvplot
#'
#' @importFrom ggtext element_markdown
#' @importFrom ggplot2 is.ggplot
#'
#' @references https://cran.r-project.org/web/packages/survminer/readme/README.html
#'
#' @export
#'
#' @examples
#'
#' #----------------------------------------------------------------------------
#' # ggsurvplot_theme() examples
#' #----------------------------------------------------------------------------
#' library(bkstat)
#' library(survminer)
#' library(survival)
#'
#' fit <- survfit(Surv(time, status) ~ sex, data = lung)
#'
#' ggsurv <- ggsurvplot(
#'   fit,                     # survfit object with calculated statistics.
#'   data = lung,             # data used to fit survival curves.
#'   risk.table = TRUE,       # show risk table.
#'   pval = TRUE,             # show p-value of log-rank test.
#'   conf.int = TRUE,         # show confidence intervals for
#'   # point estimates of survival curves.
#'   palette = c("#E7B800", "#2E9FDF"),
#'   xlim = c(0,500),         # present narrower X axis, but not affect
#'   # survival estimates.
#'   xlab = "Time in days",   # customize X axis label.
#'   break.time.by = 100,     # break X axis in time intervals by 500.
#'   ggtheme = theme_light(), # customize plot and risk table with a theme.
#'   risk.table.y.text.col = TRUE,# colour risk table text annotations.
#'   risk.table.height = 0.25, # the height of the risk table
#'   risk.table.y.text = FALSE,# show bars instead of names in text annotations
#'   # in legend of risk table.
#'   ncensor.plot = TRUE,      # plot the number of censored subjects at time t
#'   ncensor.plot.height = 0.25,
#'   conf.int.style = "step",  # customize style of confidence intervals
#'   surv.median.line = "hv",  # add the median survival pointer.
#'   legend.labs = c("Male", "Female")    # change legend labels.
#'   )
#'
#' ggsurvplot_theme(
#'   ggsurv,
#'   font.title    = c(16, "bold", "darkblue"),
#'   font.subtitle = c(15, "bold.italic", "purple"),
#'   font.caption  = c(14, "plain", "orange"),
#'   font.x        = c(14, "bold.italic", "red"),
#'   font.y        = c(14, "bold.italic", "darkred"),
#'   font.xtickslab = c(12, "plain", "darkgreen")
#' )
#'
#' ggsurv$table <- ggsurvplot_theme(
#'   ggsurv$table,
#'   font.title    = c(13, "bold.italic", "green"),
#'   font.subtitle = c(15, "bold", "pink"),
#'   font.caption  = c(11, "plain", "darkgreen"),
#'   font.x        = c(8, "bold.italic", "orange"),
#'   font.y        = c(11, "bold.italic", "darkgreen"),
#'   font.xtickslab = c(9, "bold", "red")
#' )
#'
#' ggsurv
#'
ggsurvplot_theme <- function(
  p,
  font.title = NULL,
  font.subtitle = NULL,
  font.caption = NULL,
  font.x = NULL,
  font.y = NULL,
  font.xtickslab = NULL,
  font.ytickslab = NULL
){
  original.p <- p
  if(is.ggplot(original.p)) list.plots <- list(original.p)
  else if(is.list(original.p)) list.plots <- original.p
  else stop("Can't handle an object of class ", class (original.p))
  .set_font <- function(font){
    font <- .parse_font(font)
    ggtext::element_markdown(size = font$size, face = font$face, colour = font$color)
  }
  for(i in 1:length(list.plots)){
    p <- list.plots[[i]]
    if(is.ggplot(p)){
      if(!is.null(font.title)) p <- p + theme(plot.title = .set_font(font.title))
      if(!is.null(font.subtitle)) p <- p + theme(plot.subtitle = .set_font(font.subtitle))
      if(!is.null(font.caption)) p <- p + theme(plot.caption = .set_font(font.caption))
      if(!is.null(font.x)) p <- p + theme(axis.title.x = .set_font(font.x))
      if(!is.null(font.y)) p <- p + theme(axis.title.y = .set_font(font.y))
      if(!is.null(font.xtickslab)) p <- p + theme(axis.text.x = .set_font(font.xtickslab))
      if(!is.null(font.ytickslab)) p <- p + theme(axis.text.y = .set_font(font.ytickslab))
      list.plots[[i]] <- p
    }
  }
  if(is.ggplot(original.p)) list.plots[[1]]
  else list.plots
}

# Internal ggpubr function
.parse_font <- function(font) {
  if (is.null(font))
    res <- NULL
  else if (inherits(font, "list"))
    res <- font
  else {
    size <- grep("^[0-9]+$", font, perl = TRUE)
    face <- grep("plain|bold|italic|bold.italic", font, perl = TRUE)
    if (length(size) == 0)
      size <- NULL
    else size <- as.numeric(font[size])
    if (length(face) == 0)
      face <- NULL
    else face <- font[face]
    color <- setdiff(font, c(size, face))
    if (length(color) == 0)
      color <- NULL
    res <- list(size = size, face = face, color = color)
  }
  res
}
