#' Logistic Model Predictions
#'
#' \code{stats::\link[stats]{predict}} doesn't offer confidence intervals. This
#' function will add the Wald confidence limits on the logit (log-odds) scale or
#' back-transformed to the probability scale.
#'
#' @param object A glm object with family `binomial` and link function `logit`.
#' @param newdata optionally, a data frame in which to look for variables with
#' which to predict. If omitted, the fitted linear predictors are used..
#' @param type The type of prediction required. The default, "link", is on the
#' scale of the linear predictors; the alternative "response" is on the scale of
#' the response variable. Thus for a default binomial model the default predictions
#' are of log-odds (probabilities on logit scale) and type = "response" gives
#' the predicted probabilities.
#' @param conf.level The confidence level to use.
#'
#' @seealso \code{stats::\link[stats]{predict}}
#'
#' @return data.frame
#'
#' @importFrom stats qnorm predict
#'
#' @export
#'
#' @examples
#' #----------------------------------------------------------------------------
#' # predict_glm() examples.
#' #----------------------------------------------------------------------------
#' library(bkstat)
#'
#' mod <- glm(
#'   formula = am ~ disp + vs,
#'   data = mtcars,
#'   family = binomial
#' )
#'
#' predict_glm(
#'   object = mod,
#'   type = "link"
#' )
#'
#' predict_glm(
#'   object = mod,
#'   type = "response"
#' )
#'
#' predict_glm(
#'   object = mod,
#'   newdata = data.frame(disp = c(150, 300), vs = c(0, 0)),
#'   type = "link"
#' )
#'
#' predict_glm(
#'   object = mod,
#'   newdata = data.frame(disp = c(150, 300), vs = c(0, 0)),
#'   type = "response"
#' )
#'
predict_glm <- function(
    object,
    newdata = NULL,
    type = c("link", "response"),
    conf.level = 0.95
) {
  #-----------------------------------------------------------------------------
  # Check arguments
  #-----------------------------------------------------------------------------
  stopifnot(inherits(object, "glm"))
  stopifnot(object$family$family == "binomial")
  stopifnot(object$family$link == "logit")
  type <- match.arg(type, choices = c("link", "response"), several.ok = FALSE)

  #-----------------------------------------------------------------------------
  # Get predictions
  #-----------------------------------------------------------------------------
  preds <- predict(
    object = object,
    newdata = newdata,
    type = "link",
    se.fit = TRUE
  )

  trans <- function(type) {
    if(identical(type, "link")) {
      return(function(x) {x})
    }
    if(identical(type, "response")) {
      return(function(x) {plogis(x)})
    }
  }
  trans <- trans(type)

  estimate <- trans(preds$fit)
  lwr <- trans(preds$fit + (qnorm((1 - conf.level) / 2) * preds$se.fit))
  upr <- trans(preds$fit + (qnorm(1 - (1 - conf.level) / 2) * preds$se.fit))

  out <- data.frame(
    estimate = estimate,
    lower_ci = lwr,
    upper_ci = upr
  )

  if(!is.null(newdata)) {
    out <- cbind(newdata, out, row.names = NULL)
  }

  #-----------------------------------------------------------------------------
  # Return
  #-----------------------------------------------------------------------------
  out
}
