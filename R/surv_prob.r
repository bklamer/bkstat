#' Get the survival probability at a given time
#'
#' This function has been superseded by \code{bkstat::\link[bkstat]{predict_surv_prob}}.
#' Get the survival probability at a given time. A wrapper for `survival:::summary.survfit()`.
#'
#' @param df A data frame.
#' @param formula A survival formula: `Surv(time, time2, event) ~ variable`.
#' @param times A numeric vector of times.
#' @param ... Optional arguments passed to `survival:::summary.survfit()`.
#'
#' @return data.frame
#'
#' @importFrom survival survfit
#' @importFrom bkmisc check_data_frame check_numeric check_formula
#'
#' @export
#'
#' @examples
#' #----------------------------------------------------------------------------
#' # surv_prob() examples.
#' #----------------------------------------------------------------------------
#' library(survival)
#' library(bkstat)
#'
#' plot(survfit(formula = Surv(futime, death) ~ trt, data = myeloid))
#' surv_prob(
#'   df = myeloid,
#'   formula = Surv(futime, death) ~ trt,
#'   times = c(100, 500, 2000)
#' )
#' surv_prob(
#'   df = myeloid,
#'   formula = Surv(futime, death) ~ 1,
#'   times = c(100, 500, 2000)
#' )
#' surv_prob(
#'   df = myeloid,
#'   formula = Surv(futime, death) ~ trt + sex,
#'   times = c(100, 500, 2000)
#' )
#'
surv_prob <- function(df, formula, times, ...) {
  #-----------------------------------------------------------------------------
  # Check arguments
  #-----------------------------------------------------------------------------
  check_data_frame(df)
  check_numeric(times)
  check_formula(formula)

  #-----------------------------------------------------------------------------
  # Run survival:::summary.survfit() and transform its output to a tidy format
  #-----------------------------------------------------------------------------
  tmp <- summary(survfit(formula, data = df), times = times, ...)
  out <- data.frame(
    strata = if(length(tmp$n) > 1) {tmp$strata} else {"Overall"},
    time = tmp$time,
    # this is a horrible, yet working hack to replicate sample sizes along lengths of strata...?
    n = if(length(tmp$n) > 1) {as.integer(as.character(factor(tmp$strata, labels = tmp$n)))} else {tmp$n},
    n_at_risk = tmp$n.risk,
    n_events = tmp$n.event,
    n_censored = tmp$n.censor,
    surv_prob = tmp$surv,
    lower_ci = tmp$lower,
    upper_ci = tmp$upper,
    ci_level = tmp$conf.int,
    ci_type = tmp$conf.type,
    stringsAsFactors = FALSE
  )

  #-----------------------------------------------------------------------------
  # Return
  #-----------------------------------------------------------------------------
  out
}
