#' Dichotomize continuous variables
#'
#' Converts continuous variables into a factor that is split at a chosen cutpoint.
#'
#' @param x A numeric vector.
#' @param location A function which will be passed `x` and returns a numeric of length one, or a numeric of length one.
#' @param cutpoint A character string of either `"below"` or `"above"`.
#' @param less_than_symbol A named character vector of symbols to use for values less than the chosen location. Includes values appropriate for both `"below"` and `"above"` cutpoints.
#' @param greater_than_symbol A named character vector of symbols to use for values greater than the chosen location. Includes values appropriate for both `"below"` and `"above"` cutpoints.
#' @param digits An integer to use for rounding the output.
#'
#' @return factor with 2 levels
#'
#' @importFrom bkmisc check_numeric check_string check_character check_x_in_y
#'
#' @export
#'
#' @examples
#' #----------------------------------------------------------------------------
#' # dichotomize() examples
#' #----------------------------------------------------------------------------
#' library(bkstat)
#'
#' x <- rnorm(10)
#' dichotomize(x, cutpoint = "below")
#' dichotomize(x, cutpoint = "above")
#'
dichotomize <- function(
  x,
  location = function(x) median(x, na.rm = TRUE),
  cutpoint = "below",
  less_than_symbol = c(below = "<", above = "\u2264"),
  greater_than_symbol = c(above = ">", below = "\u2265"),
  digits = 2
) {
  #-----------------------------------------------------------------------------
  # Check arguments
  #-----------------------------------------------------------------------------
  check_numeric(x)
  check_string(cutpoint)
  check_x_in_y(x = cutpoint, y = c("below", "above"))
  check_character(less_than_symbol)
  check_character(greater_than_symbol)

  if(is.function(location)) {
    location <- location(x)
  }
  check_numeric(location)

  if(cutpoint == "below") {
    is_below <- x < location
  } else {
    is_below <- x <= location
  }

  if(!is.null(digits)) {
    check_numeric(digits)
    location <- round(location, digits)
  }

  #-----------------------------------------------------------------------------
  # Return
  #-----------------------------------------------------------------------------
  factor(
    x = ifelse(
      test = is_below,
      yes = paste0(less_than_symbol[cutpoint], location),
      no = paste0(greater_than_symbol[cutpoint], location)
    ),
    levels = c(
      paste0(less_than_symbol[cutpoint], location),
      paste0(greater_than_symbol[cutpoint], location)
    )
  )
}
