#' Convert probabilities to logits
#'
#' Converts a vector of probabilities to logits.
#'
#' @param prob A numeric vector of probabilities. 0 <= prob <= 1.
#'
#' @return numeric vector
#'
#' @importFrom stats qlogis
#' @importFrom bkmisc check_numeric
#'
#' @export
#'
#' @examples
#' #----------------------------------------------------------------------------
#' # logit() examples
#' #----------------------------------------------------------------------------
#' library(bkstat)
#'
#' prob <- c(0, 0.5, 1)
#' logit(prob)
#'
logit <- function(prob) {
  #-----------------------------------------------------------------------------
  # Check arguments
  #-----------------------------------------------------------------------------
  check_numeric(prob)

  #-----------------------------------------------------------------------------
  # Return
  # logit(p) = log(p / (1-p))
  #-----------------------------------------------------------------------------
  qlogis(prob)
}
