#' Predicted survival probability at a given time
#'
#' Get the predicted survival probability at a given time. A convenience wrapper
#' for \code{survival::\link[survival]{summary.survfit}}.
#'
#' @param object An object of either
#' - \code{survival::\link[survival]{coxph}}, or
#' - Survival formula using \code{survival::\link[survival]{Surv}}
#' @param df A data frame in which to interpret the variables in `object`.
#' - For `object = survival::coxph`, may be `NULL` or the usual `newdata` style argument.
#' - For `object = formula`, must be the data frame used for fitting in
#' \code{survival::\link[survival]{survfit}}.
#' @param conf.level The level for a two-sided confidence interval on the survival
#' curve(s). Default is 0.95.
#' @param times A numeric vector of times.
#' @param scale A numeric value to rescale the survival time, e.g., if the input
#' data to survfit were in days, scale = 365.25 would scale the output to years.
#' @param extend `TRUE` or `FALSE`. if TRUE, prints information for all specified
#' `times`, even if there are no subjects left at the end of the specified `times`.
#' @param ... Optional arguments passed to \code{survival::\link[survival]{survfit}}.
#'
#' @return data.frame
#'
#' @importFrom survival survfit
#' @importFrom bkmisc check_data_frame check_numeric check_probability pretty_print
#' @importFrom dplyr arrange across any_of mutate where relocate
#' @importFrom tidyr separate_longer_delim separate_wider_delim pivot_wider unnest
#'
#' @export
#'
#' @examples
#' #----------------------------------------------------------------------------
#' # predict_surv_prob() examples.
#' #----------------------------------------------------------------------------
#' library(survival)
#' library(bkstat)
#'
#' bkstat::predict_surv_prob(
#'   object = Surv(time, status) ~ 1,
#'   df = lung,
#'   times = c(12, 48),
#'   extend = TRUE
#' )
#'
#' bkstat::predict_surv_prob(
#'   object = coxph(Surv(time, status) ~ 1, lung),
#'   times = c(12, 48)
#' )
#'
#' bkstat::predict_surv_prob(
#'   object = Surv(time, status) ~ sex,
#'   df = lung,
#'   times = c(12, 48),
#'   extend = TRUE
#' )
#'
#' bkstat::predict_surv_prob(
#'   object = coxph(Surv(time, status) ~ sex, lung),
#'   df = data.frame(expand.grid(sex = 1:2)),
#'   times = c(12, 48)
#' )
#'
#' bkstat::predict_surv_prob(
#'   object = Surv(time, status) ~ sex + ph.ecog,
#'   df = lung,
#'   times = 48,
#'   extend = TRUE
#' )
#'
#' bkstat::predict_surv_prob(
#'   object = coxph(Surv(time, status) ~ sex + ph.ecog, lung),
#'   df = data.frame(expand.grid(sex = 1:2, ph.ecog = 0:3)),
#'   times = 48
#' )
#'
#' bkstat::predict_surv_prob(
#'   object = coxph(Surv(time, status) ~ 1, lung),
#'   df = data.frame(expand.grid(sex = 1:2, ph.ecog = 0:3)),
#'   times = c(12, 48)
#' )
#'
predict_surv_prob <- function(
  object,
  df = NULL,
  conf.level = 0.95,
  times,
  scale = 1,
  extend = FALSE,
  ...
) {
  #-----------------------------------------------------------------------------
  # Check arguments
  #-----------------------------------------------------------------------------
  if(!inherits(object, c('formula', 'coxph', 'survreg', 'aareg'))) {
    stop(
      "Check the 'object' argument in function bkstat::predict_surv_prob(). 'object' has class ", pretty_print(class(arg)), ", but needs to be class 'formula' or a survival model object.",
      call. = FALSE
    )
  }
  if(is.null(df) & inherits(object, "formula")) {
    stop("Check argument 'df' in bkstat::predict_surv_prob(). You must pass a data frame for fitting when using a formula 'object'.")
  }
  if(!is.null(df)) {check_data_frame(df)}
  check_probability(conf.level)
  check_numeric(times)
  check_numeric(scale)

  #-----------------------------------------------------------------------------
  # Function to convert survival::summary.survfit to a data.frame.
  #-----------------------------------------------------------------------------
  summary_survfit_coxph <- function(x, df) {
    out <- lapply(seq_along(x$time), function(i) {
      data.frame(
        time = x$time[i],
        n = x$n,
        n_at_risk = x$n.risk[i],
        n_events = x$n.event[i],
        surv_prob = as.matrix(x$surv)[i,],
        lower_ci = as.matrix(x$lower)[i,],
        upper_ci = as.matrix(x$upper)[i,],
        ci_level = x$conf.int,
        ci_type = x$conf.type,
        estimator = "Cox Proportional Hazards",
        stringsAsFactors = FALSE
      )
    })

    out <- do.call('rbind', out)

    if(is.null(df)) { # Overall prediction
      out |>
        dplyr::arrange(time)
    } else { # Conditional predictions
      cbind(df, out, row.names = NULL) |>
        dplyr::arrange(time, dplyr::across(dplyr::any_of(names(df))))
    }
  }

  summary_survfit_formula <- function(x) {
    out <- data.frame(
      strata = if(length(x$n) > 1) {x$strata} else {"Overall"},
      time = x$time,
      # this is a horrible, yet working hack to replicate sample sizes along lengths of strata...?
      n = if(length(x$n) > 1) {as.integer(as.character(factor(x$strata, labels = x$n)))} else {x$n},
      n_at_risk = x$n.risk,
      n_events = x$n.event,
      n_censored = x$n.censor,
      surv_prob = x$surv,
      lower_ci = x$lower,
      upper_ci = x$upper,
      ci_level = x$conf.int,
      ci_type = x$conf.type,
      estimator = "Kaplan-Meier",
      stringsAsFactors = FALSE
    )

    if(length(x$n) == 1) { # overall model
      out$strata <- NULL
      out
    } else { # stratified model
      out <- out |>
        tidyr::separate_longer_delim(cols = "strata", delim = ",") |>
        tidyr::separate_wider_delim(cols = "strata", delim = "=", names = c("variable", "value")) |>
        dplyr::mutate(variable = trimws(variable), value = trimws(value))
      names <- unique(out$variable)
      # The pivot needs to account for situations when probs are missing values.
      # It results in list-cols, so unnesting is required.
      out <- out |>
        tidyr::pivot_wider(names_from = "variable", values_from = "value", values_fn = list) |>
        tidyr::unnest(cols = dplyr::where(is.list)) |>
        dplyr::relocate(dplyr::all_of(names), .before = 1) |>
        dplyr::arrange(time, dplyr::across(dplyr::all_of(names))) |>
        as.data.frame()
      out
    }
  }

  #-----------------------------------------------------------------------------
  # Run survival::summary.survfit() and transform its output to a tidy format
  #-----------------------------------------------------------------------------
  if(inherits(object, "coxph")) {
    if(is.null(df)) {
      sf <- survfit(formula = object, conf.int = conf.level, ...)
    } else {
      sf <- survfit(formula = object, newdata = df, conf.int = conf.level, ...)
    }
  } else {
    sf <- survfit(formula = object, data = df, conf.int = conf.level, ...)
  }

  ssf <- summary(sf, times = times, scale = scale, extend = extend)

  if(inherits(object, "coxph")) {
    out <- summary_survfit_coxph(x = ssf, df = df)
  } else {
    out <- summary_survfit_formula(x = ssf)
  }

  #-----------------------------------------------------------------------------
  # Return
  #-----------------------------------------------------------------------------
  out
}
