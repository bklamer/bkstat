#' Relative/Percent Difference in Means
#'
#' Calculate the relative difference in means with standard error based on the
#' delta method.
#'
#' For two samples x and y, relative difference = (mean(y) - mean(x)) / mean(x)
#'
#' @param x A numeric vector
#' @param y A numeric vector
#' @param scale A numeric value to scale the output. `scale=100` produces percentages
#' and `scale=1` produces proportions.
#'
#' @return data.frame
#'
#' @importFrom bkmisc check_numeric
#'
#' @export
#'
#' @examples
#' #----------------------------------------------------------------------------
#' # reldif_means() examples
#' #----------------------------------------------------------------------------
#' library(bkstat)
#'
#' x <- rnorm(30, mean = 10, sd = 3)
#' y <- rnorm(30, mean = 8, sd = 2.5)
#'
#' reldif_means(x,y)
#'
reldif_means <- function(x, y, scale = 100) {
  #-----------------------------------------------------------------------------
  # Check arguments
  #-----------------------------------------------------------------------------
  check_numeric(x)
  check_numeric(y)

  #-----------------------------------------------------------------------------
  # Calculate relative difference in means
  # https://www2.census.gov/programs-surveys/acs/tech_docs/accuracy/percchg.pdf
  # https://stats.stackexchange.com/q/376639
  # https://stats.stackexchange.com/a/64658
  #-----------------------------------------------------------------------------
  x <- x[!is.na(x)]
  y <- y[!is.na(y)]

  xn <- length(x)
  yn <- length(y)

  xbar = mean(x)
  ybar = mean(y)

  xs = sd(x)
  ys = sd(y)

  xse <- xs / sqrt(xn)
  yse <- ys / sqrt(yn)

  reldif = scale * ((ybar - xbar) / xbar)

  # Analytic solutions
  reldif_se = scale * abs(ybar/xbar) * sqrt((xse^2 / xbar^2) + (yse^2 / ybar^2))
  # If x and y are not independent, then use
  # scale * sqrt(((yse^2 * xbar^2) - 2*cov(x,y) + (xse^2 * ybar^2)) / xbar^4)
  #reldif_se_2 = scale * sqrt(((yse^2 * xbar^2) + (xse^2 * ybar^2)) / xbar^4)

  # delta method solution
  # The slope coefficient is equal to ybar - xbar, the intercept coefficient is
  # equal to xbar, thus ~x2/x1 provides us the estimate of interest.
  #y <- c(y, x)
  #x <- c(rep("y", yn), rep("x", xn))
  #fit <- lm(y ~ x)
  #reldif_se_3 <- msm::deltamethod(~x2/x1, mean = coef(fit), cov = vcov(fit))

  #stopifnot(all.equal(reldif_se, reldif_se_2, tolerance = scale*0.01))
  #stopifnot(all.equal(reldif_se, reldif_se_3, tolerance = scale*0.01))

  #-----------------------------------------------------------------------------
  # Return
  #-----------------------------------------------------------------------------
  data.frame(
    estimate = reldif,
    se = reldif_se
  )
}
