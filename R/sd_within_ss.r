#' Sample size for the within subject standard deviation
#'
#' Estimate the required sample size for the within subject standard deviation
#' in a repeatability or reproducibility study.
#'
#' Assumptions:
#'
#' - the within-subject standard deviation is the same throughout the range
#' - to estimate the standard error, the distribution of observations within the
#'   subject is Normal
#' - there are equal numbers of observations on each subject
#'
#' @param n An integer for the number of subjects.
#' @param m An integer for the number of observations per subject.
#' @param precision A numeric for the relative precision around the standard deviation.
#' @param conf_level A numeric for the confidence level.
#'
#' @return list
#'
#' @importFrom stats qnorm
#'
#' @references
#' \url{https://www-users.york.ac.uk/~mb55/meas/sizerep.htm}
#'
#' \url{https://www-users.york.ac.uk/~mb55/meas/seofsw.htm}
#'
#' \insertRef{bland_1996}{bkstat}
#'
#' @export
#'
#' @examples
#' #----------------------------------------------------------------------------
#' # sd_within_ss() examples
#' #----------------------------------------------------------------------------
#' library(bkstat)
#'
#' sd_within_ss(m = 11, precision = 0.1)
#' sd_within_ss(n = 20, m = 11)
#' sd_within_ss(m = 5, precision = 0.1)
#' sd_within_ss(n = 5, m = 11, precision = 0.1)
#'
#' res <- expand.grid(
#'   n = seq(from = 10, to = 50, by = 5),
#'   m = seq(from = 2, to = 5, by = 1),
#'   precision = seq(from = 0.1, to = 0.25, by = 0.05)
#' ) |>
#'   dplyr::rowwise() |>
#'   dplyr::mutate(
#'     sd_within_ss(n, m, precision)$n,
#'     sd_within_ss(n, m, precision)$m,
#'     sd_within_ss(n, m, precision)$precision,
#'   ) |>
#'   dplyr::select(n, m, precision, required_n, required_m, relative_precision)
#'
#' res
#'
sd_within_ss <- function(n, m, precision, conf_level = 0.95) {
  n_missing <- missing(n)
  m_missing <- missing(m)
  precision_missing <- missing(precision)
  all_specified <- !n_missing & !m_missing & !precision_missing

  q <- qnorm(1 - ((1 - conf_level) / 2))
  ret <- list()

  if(n_missing | all_specified) {
    nss = q^2 / (2 * precision^2 * (m-1))
    ret$n <- data.frame(
      chosen_m = m,
      chosen_precision = precision,
      required_n = ceiling(nss)
    )
  }

  if(m_missing | all_specified) {
    mss = 1 + (q^2 / (precision^2 * 2 * n))
    ret$m <- data.frame(
      chosen_n = n,
      chosen_precision = precision,
      required_m = ceiling(mss)
    )
  }

  if(precision_missing | all_specified) {
    precisionss = q / sqrt(2 * n * (m-1))
    ret$precision <- data.frame(
      chosen_n = n,
      chosen_m = m,
      relative_precision = precisionss
    )
  }

  ret
}
