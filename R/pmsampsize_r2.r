#' Estimate Cox-Snell R2 using AUC
#'
#' Estimates the Cox-Snell R2 from a reported C-statistic of a prediction
#' model for a binary outcome. Useful for sample size calculation as seen in
#' \code{pmsampsize::\link[pmsampsize]{pmsampsize}}.
#'
#'
#' @param auc A numeric for the existing model's reported AUC.
#' @param prop A numeric for the outcome proportion.
#' @param n An integer for the simulation size.
#'
#' @return A list with two elements: R2 Nagelkerke and R2 Cox-Snell estimates.
#'
#' @importFrom rms lrm
#' @importFrom stats qnorm rnorm
#'
#' @references
#' Riley, R. D., Calster, B., & Collins, G. S. (2020). A note on estimating the Cox-Snell R 2 from a reported C statistic ( AUROC ) to inform sample size calculations for developing a prediction model with a binary outcome. Statistics in Medicine. https://doi.org/10.1002/sim.8806
#'
#' Riley, R. D., Snell, K. I., Ensor, J., Burke, D. L., Harrell Jr, F. E., Moons, K. G., & Collins, G. S. (2018). Minimum sample size for developing a multivariable prediction model: PART II - binary and time-to-event outcomes. Statistics in Medicine. https://doi.org/10.1002/sim.7992
#'
#' @export
#'
#' @examples
#'
#' #----------------------------------------------------------------------------
#' # pmsampsize_r2() examples
#' #----------------------------------------------------------------------------
#' library(bkstat)
#'
#' set.seed(1234)
#' pmsampsize_r2(auc = 0.81, prop = 0.77, n = 1000000)
#'
pmsampsize_r2 <- function(auc, prop, n = 1000000){
  # Define mu as a function of the C statistic
  mu <- sqrt(2) * qnorm(auc)
  # Simulate large sample linear prediction based on two normals
  # for non-events: N(0, 1) and events: N(mu, 1)
  LP <- c(rnorm(prop*n,  mean=0, sd=1), rnorm((1-prop)*n, mean=mu, sd=1))
  y <- c(rep(0, prop*n), rep(1, (1-prop)*n))
  # Fit a logistic regression with LP as covariate;
  # this is essentially a calibration model, and the intercept and
  # slope estimate will ensure the outcome proportion is accounted
  # for, without changing C statistic
  fit <- lrm(y~LP)
  max_R2 <- function(prop){
    1-(prop^prop*(1-prop)^(1-prop))^2
  }

  list(
    R2.nagelkerke = as.numeric(fit$stats['R2']),
    R2.coxsnell = as.numeric(fit$stats['R2']) * max_R2(prop)
  )
}

