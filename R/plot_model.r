#' Create a coefficient/model/forest plot
#'
#' Create a coefficient/model/forest plot. I wasn't satisfied with other solutions
#' already floating around, so put this together. It requires some manual intervention
#' but can produce a nice result.
#'
#' @param df A data.frame of model results. It must contain the following columns:
#' \itemize{
#'   \item `predictor` - Fill with character vector of predictor variable names.
#'   \item `estimate_label` - Fill with character vector of coefficient estimates and CI values.
#'   \item `estimate` - Fill with numeric vector of coefficient estimates.
#'   \item `lower` - Fill with numeric vector of coefficient lower CI estimates.
#'   \item `upper` - Fill with numeric vector of coefficient upper CI estimates.
#' }
#' @param vline An integer for the location of the vertical line related to hypothesis test for coefficients.
#' @param xlab A character string for the x-axis label.
#' @param xbreaks A numeric vector of x-axis breaks. (passed to `scale_x_continuous()`)
#' @param xlim A numeric vector (length 2) for the x-axis limits. (passed to `scale_x_continuous()`)
#' @param trans A string for the transformation to apply to the x-axis. Usually
#' want `"identity"` or `"log"`. See `?ggplot2::scale_x_continuous` for more information.
#' @param theme ggplot2 theme.
#' @param rel_widths A numeric vector for the relative widths of the 3 plots that form the final plot.
#' @param colname_lines A numeric vector of 2 values. Controls the position of the horizontal column name under lines.
#'
#' @return ggplot
#'
#' @importFrom ggplot2 ggplot aes geom_errorbarh geom_vline geom_point scale_y_continuous
#'                     scale_x_continuous coord_cartesian theme theme_bw theme_set
#'                     element_text element_blank labs geom_text geom_hline xlim
#' @importFrom grid unit
#' @importFrom scales pretty_breaks
#' @importFrom cowplot plot_grid
#' @importFrom ggtext geom_richtext
#' @importFrom bkmisc check_data_frame check_numeric check_string check_function_or_numeric
#'
#' @export
#'
#' @examples
#'
#' #----------------------------------------------------------------------------
#' # plot_model() examples
#' #----------------------------------------------------------------------------
#' library(bkstat)
#'
#' df <- data.frame(
#' predictor = c(
#'   "Predictors",
#'   "  Sociodemographic",
#'   "    - Age",
#'   "      Sex:",
#'   "      - Male",
#'   "      - Female",
#'   "  Clinical",
#'   "    - Blood Pressure",
#'   "    - Height"),
#' estimate_label = c(
#'   "Estimate (95% CI)",
#'   NA,
#'   "1 (.3, 1.5)",
#'   NA,
#'   "Reference",
#'   "2 (1, 3)",
#'   NA,
#'   "3.3 (3, 4.5)",
#'   "2.5 (2.1, 2.7)"
#' ),
#' estimate = c(NA, NA, 1, NA, NA, 2, NA, 3.3, 2.5),
#' lower = c(NA, NA, .3, NA, NA, 1, NA, 3, 2.1),
#' upper = c(NA, NA, 1.5, NA, NA, 3, NA, 4.5, 2.7)
#' )
#'
#' plot_model(
#'   df = df,
#'   vline = 1,
#'   xlab = "Odds ratio",
#'   xbreaks = c(0.3, 0.5, 1, 2, 3, 4, 5),
#'   xlim = c(0.25, 5),
#'   trans = "log"
#'  )
#'
plot_model <- function(
  df,
  vline = NULL,
  xlab = NULL,
  xbreaks = scales::pretty_breaks(),
  xlim = NULL,
  trans = "identity",
  theme = ggplot2::theme_bw(),
  rel_widths = c(0.15, 0.15, 0.7),
  colname_lines = c(-0.45, NA)
) {
  # Todo: plot multiple model results?
  #-----------------------------------------------------------------------------
  # Check arguments
  #-----------------------------------------------------------------------------
  check_data_frame(df)
  check_numeric(vline)
  check_string(xlab)
  check_function_or_numeric(xbreaks)
  check_numeric(xlim)
  check_string(trans)
  check_numeric(rel_widths)
  check_numeric(colname_lines)

  #-----------------------------------------------------------------------------
  # Create plots
  #-----------------------------------------------------------------------------
  # Set a y-axis value for each label. A decreasing sequence of integers produces
  # the correct order.
  df$yval <- rev(seq_len(nrow(df)))

  # ggplot theme
  theme_set(theme)

  # coefficient plot
  plot_model <- ggplot(df, aes(x = estimate, xmin = lower, xmax = upper, y = yval)) +
    geom_errorbarh(height = 0.2, color = 'red') +
    geom_vline(xintercept = vline) +
    geom_point() +
    scale_y_continuous(breaks = df$yval[which(!is.na(df$estimate))], expand = c(0, 0)) +
    scale_x_continuous(breaks = xbreaks, limits = xlim, trans = trans) + # will clip unseen data points
    coord_cartesian(ylim = c(0, nrow(df) + 1)) + # Could use ylim(), scale_y_continuous(), coord_cartesian(). coord_cartesian() does not clip (clipping removes unseen data points)
    theme(
      axis.text.y = element_blank(),
      axis.title.y = element_blank(),
      #axis.ticks.y = element_blank(),
      #panel.border = element_blank(),
      #panel.grid.major.y = element_blank(),
      panel.grid.minor.y = element_blank(),
      plot.margin = unit(c(5.5, 5.5, 5.5, 0), "pt") # top, right, bottom, and left
    )  +
    labs(x = xlab)

  # labels, can be modified manually for different situations.
  pred_table <- ggplot(df, aes(y = yval)) +
    # Unfortunately this does not allow spaces before text.
    # ggtext::geom_richtext(
    #   mapping = aes(label = predictor, x = 0),
    #   hjust = 0,
    #   fill = NA,
    #   label.color = NA, # remove background and outline
    #   label.padding = grid::unit(c(0, 0, 0, 0), "pt") # top, right, bottom, and left
    # ) +
    geom_text(aes(label = predictor, x = 0), hjust = 0) +
    geom_hline(aes(yintercept = nrow(df) + colname_lines[1])) +
    geom_hline(aes(yintercept = nrow(df) + colname_lines[2])) +
    scale_y_continuous(expand = c(0, 0), limits = c(0, nrow(df) + 1)) +
    theme(
      axis.text = element_blank(),
      axis.title = element_blank(),
      axis.ticks = element_blank(),
      panel.border = element_blank(),
      panel.grid = element_blank(),
      plot.margin = grid::unit(c(0, 5.5, 0, 5.5), "pt") # top, right, bottom, and left
    ) +
    xlim(0, 5)

  est_table <- ggplot(df, aes(y = yval)) +
    # Unfortunately this does not allow spaces before text.
    # ggtext::geom_richtext(
    #   mapping = aes(label = estimate_label, x = 0),
    #   hjust = 0,
    #   fill = NA,
    #   label.color = NA, # remove background and outline
    #   label.padding = grid::unit(c(0, 0, 0, 0), "pt") # top, right, bottom, and left
    # ) +
    geom_text(aes(label = estimate_label, x = 0), hjust = 0) +
    geom_hline(aes(yintercept = nrow(df) + colname_lines[1])) +
    geom_hline(aes(yintercept = nrow(df) + colname_lines[2])) +
    scale_y_continuous(expand = c(0, 0), limits = c(0, nrow(df) + 1)) +
    theme(
      axis.text = element_blank(),
      axis.title = element_blank(),
      axis.ticks = element_blank(),
      panel.border = element_blank(),
      panel.grid = element_blank(),
      plot.margin = unit(c(0, 0, 0, 0), "pt") # top, right, bottom, and left
    ) +
    xlim(0, 5)

  #-----------------------------------------------------------------------------
  # Return
  #-----------------------------------------------------------------------------
  plot_grid(
    pred_table,
    est_table,
    plot_model,
    align = "h",
    axis = "bt",
    nrow = 1,
    rel_widths = rel_widths
  )
}
