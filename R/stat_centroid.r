#' @title Connect points to their centroid
#'
#' @description Connect points to their centroid.
#'
#' @param mapping Set of aesthetic mappings created by `aes()` or `aes_()`. If specified and `inherit.aes = TRUE` (the default), it is combined with the default mapping at the top level of the plot. You must supply mapping if there is no plot mapping.
#' @param data The data to be displayed in this layer. If NULL, the default, the data is inherited from the plot data as specified in the call to `ggplot()`.
#' @param geom Use to override the default Geom
#' @param position Use to override the default position
#' @param na.rm If `FALSE`, the default, missing values are removed with a warning. If `TRUE`, missing values are silently removed.
#' @param show.legend Logical. Should this layer be included in the legends? `NA`, the default, includes if any aesthetics are mapped. `FALSE` never includes, and `TRUE` always includes. It can also be a named logical vector to finely select the aesthetics to display.
#' @param inherit.aes `TRUE` or `FALSE`. If `FALSE`, overrides the default aesthetics, rather than combining with them. This is most useful for helper functions that define both data and aesthetics and shouldn't inherit behaviour from the default plot specification.
#' @param cfun Function used to calculate centroid location.
#' @param ... other arguments.
#'
#' @importFrom ggplot2 ggproto Stat layer
#'
#' @export
#'
#' @examples
#' #----------------------------------------------------------------------------
#' # stat_centroid() examples
#' #----------------------------------------------------------------------------
#' library(bkstat)
#' library(ggplot2)
#'
#' df <- data.frame(
#'   group = c(rep("a", 5), rep("b", 5)),
#'   x = rnorm(10),
#'   y = rnorm(10)
#' )
#'
#' ggplot(df, aes(x = x, y = y, color = group)) +
#'   geom_point() +
#'   stat_centroid()
#'
stat_centroid <- function(
  mapping = NULL,
  data = NULL,
  geom = "segment",
  position = "identity",
  na.rm = FALSE,
  show.legend = NA,
  inherit.aes = TRUE,
  cfun = mean,
  ...
) {
  layer(
    stat = StatCentroid,
    data = data,
    mapping = mapping,
    geom = geom,
    position = position,
    show.legend = show.legend,
    inherit.aes = inherit.aes,
    params = list(na.rm = na.rm, cfun = cfun, ...)
  )
}

#' @title 'ggproto' objects
#'
#' @description Internal definitions of 'ggproto' objects needed for geoms and stats.
#'
#' @rdname bkstat-ggproto
#' @format NULL
#' @usage NULL
#' @keywords internal
#'
#' @export
StatCentroid <- ggplot2::ggproto(
  `_class` = "StatCentroid",
  `_inherit` = ggplot2::Stat,
  compute_group = function(data, scales, params, cfun = mean) {
    data$xend <- cfun(data$x)
    data$yend <- cfun(data$y)
    return(data)
  },
  required_aes = c("x", "y")
)
