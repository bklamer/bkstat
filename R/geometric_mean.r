#' Geometric mean
#'
#' Calculate the geometric mean.
#'
#' The geometric mean is calculated as
#'
#' \deqn{
#'   (\prod{x_{i}})^{\frac{1}{n}} =
#'   \sqrt[n]{x_{1} x_{2} x_{3} \cdots x_{n}} =
#'   e^{\text{arithmetic mean}(ln(x))}
#' }
#'
#' thus is undefined for negative values (`NaN`) and if any value is zero
#' (i.e. -100%) the geometric mean will result in zero.
#'
#' The geometric mean is useful for data which spreads several orders of magnitude,
#' ratios, percentages, and other scales bounded by zero. It should not be used
#' on data which is already log transformed such as pH or decibels (db).
#'
#' It's common to work with values which form a sequence of percent change
#' (i.e. 1%, 3%, 0%, -2%, 5%, ...). The geometric mean for this sequence should
#' be calculated on the decimal multiplier equivalent values which can be calculated
#' by adding one to the decimal change (i.e. 1.01, 1.03, 1, 0.98, 1.05, ...). You
#' may then subtract one from the geometric mean to convert back to the percent
#' change.
#'
#' @param x A numeric vector
#' @param na.rm `TRUE` or `FALSE`. If `TRUE`, `NA` values are removed prior to computation.
#'
#' @return Numeric vector
#'
#' @importFrom bkmisc check_numeric check_logical
#'
#' @export
#'
#' @examples
#' #----------------------------------------------------------------------------
#' # geometric_mean() examples
#' #----------------------------------------------------------------------------
#' library(bkstat)
#'
#' geometric_mean(-1)
#' geometric_mean(c(1.03, 1.04, 0))
#' geometric_mean(c(1.02, 1.03, 1.04))
#'
geometric_mean <- function(x, na.rm = TRUE) {
  #-----------------------------------------------------------------------------
  # Check arguments
  #-----------------------------------------------------------------------------
  check_numeric(x)
  check_logical(na.rm)

  #-----------------------------------------------------------------------------
  # Calculate geometric mean
  #-----------------------------------------------------------------------------
  # Geometric mean is not defined for values less than zero
  if(any(x < 0, na.rm = TRUE)) {
    return(NaN)
  }

  # If zero (e.g. -100%) is in the vector, then geometric mean is zero
  if(any(x == 0, na.rm = TRUE)) {
    return(0)
  }

  exp(mean(log(x), na.rm = na.rm))
}
