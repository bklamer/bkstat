#' Classification accuracy and agreement estimates
#'
#' Classification accuracy and agreement estimates for 2x2 contingency tables.
#'
#' The table must be in the following format for correct results:
#'
#' |            | actual + | actual - |
#' |------------|----------|----------|
#' | estimate + | a        | b        |
#' | estimate - | c        | d        |
#'
#' Wilson confidence intervals are returned for the proportion estimates.
#' Functions from the `DescTools` package are used for confidence intervals.
#' Estimates returned include:
#'
#' - Estimated prevalence: The estimated proportion of subjects with positive outcome (based on the estimated label).
#' - Actual prevalence: The actual proportion of subjects with positive outcome (based on the actual label).
#' - Estimated accuracy: Among all subjects, the proportion who had a correct classification.
#' - Sensitivity: Among subjects with actual positive outcome, the proportion that were correctly classified with positive outcome.
#' - Specificity: Among subjects with actual negative outcome, the proportion that were correctly classified with negative outcome.
#' - Positive predictive value: Among subjects with estimated positive outcome, the proportion that were correctly classified with positive outcome. (Accuracy of a positive test)
#' - Negative predictive value: Among subjects with estimated negative outcome, the proportion that were correctly classified with negative outcome. (Accuracy of a negative test)
#' - Proportion of false positives: Among subjects with actual negative outcome, the proportion that were incorrectly classified as positive outcome.
#' - Proportion of false negative: Among subjects with actual positive outcome, the proportion that were incorrectly classified as negative outcome.
#' - Cohen's Kappa: A measure of agreement between the two tests that adjusts for the amount of agreement that would be expected due to chance alone.
#'
#' @param x A table in the correct format. See details.
#' @param conf.level The confidence level as a proportion.
#'
#' @return data.frame
#'
#' @importFrom DescTools BinomCI CohenKappa
#' @importFrom bkmisc check_table check_probability
#'
#' @export
#'
#' @examples
#'
#' #----------------------------------------------------------------------------
#' # class_acc() examples
#' #----------------------------------------------------------------------------
#' library(bkstat)
#'
#' # simulate random data
#' new_test <- sample(
#'   x = c("positive", "negative"),
#'   size = 200,
#'   replace = TRUE,
#'   prob = c(0.25, 0.75)
#' )
#' standard_test <- sample(
#'   x = c("positive", "negative"),
#'   size = 200,
#'   replace = TRUE,
#'   prob = c(0.25, 0.75)
#' )
#'
#' # ensure correct matrix structure order
#' new_test <- factor(new_test, levels = c("positive", "negative"))
#' standard_test <- factor(standard_test, levels = c("positive", "negative"))
#'
#' tab <- table(new_test = new_test, standard_test = standard_test)
#'
#' # results
#' class_acc(x = tab)
#'
class_acc <- function(x, conf.level = 0.95) {
  #-----------------------------------------------------------------------------
  # Check arguments
  #-----------------------------------------------------------------------------
  check_table(x)
  check_probability(conf.level)

  #-----------------------------------------------------------------------------
  # calculate estimates
  #-----------------------------------------------------------------------------
  a <- x[1]
  b <- x[3]
  c <- x[2]
  d <- x[4]
  N <- sum(x)

  # Apparent prevalence
  aprev <- BinomCI(
    x = a + b,
    n = N,
    conf.level = conf.level,
    sides = "two.sided",
    method = "wilson"
  )
  # True prevalence
  tprev <- BinomCI(
    x = a + c,
    n = N,
    conf.level = conf.level,
    sides = "two.sided",
    method = "wilson"
  )
  # Sensitivity
  sens <- BinomCI(
    x = a,
    n = a + c,
    conf.level = conf.level,
    sides = "two.sided",
    method = "wilson"
  )
  # Specificity
  spec <- BinomCI(
    x = d,
    n = b + d,
    conf.level = conf.level,
    sides = "two.sided",
    method = "wilson"
  )
  # Overall estimated accuracy
  est_acc <- BinomCI(
    x = a + d,
    n = N,
    conf.level = conf.level,
    sides = "two.sided",
    method = "wilson"
  )
  # Positive predictive value
  ppv <- BinomCI(
    x = a,
    n = a + b,
    conf.level = conf.level,
    sides = "two.sided",
    method = "wilson"
  )
  # Negative predictive value
  npv <- BinomCI(
    x = d,
    n = c + d,
    conf.level = conf.level,
    sides = "two.sided",
    method = "wilson"
  )
  # Proportion of false positives
  pfp <- BinomCI(
    x = b,
    n = b + d,
    conf.level = conf.level,
    sides = "two.sided",
    method = "wilson"
  )
  # Proportion of false negatives
  pfn <- BinomCI(
    x = c,
    n = a + c,
    conf.level = conf.level,
    sides = "two.sided",
    method = "wilson"
  )
  # Cohens kappa
  kappa <- CohenKappa(
    x = x,
    weights = "Unweighted",
    conf.level = conf.level
  )

  res <- data.frame(
    summary = c(
      "Apparent prevalance",
      "True prevalance",
      "Diagnostic accuracy",
      "Sensitivity",
      "Specificity",
      "Positive predictive value",
      "Negative predictive value",
      "Proportion of false positives",
      "Proportion of false negatives",
      "Cohen's kappa"
    ),
    rbind(
      aprev,
      tprev,
      est_acc,
      sens,
      spec,
      ppv,
      npv,
      pfp,
      pfn,
      kappa
    ),
    row.names = NULL
  )

  #-----------------------------------------------------------------------------
  # Return
  #-----------------------------------------------------------------------------
  names(res) <- c("summary", "estimate", "lower_ci", "upper_ci")
  res
}
