#' Fast t.test
#'
#' A fast t-test. Returns only the p-value. No argument checks are done and assumes
#' no missing values.
#'
#' @param x A numeric vector for sample 1.
#' @param y A numeric vector for sample 2.
#' @param alternative A character string specifying the alternative hypothesis,
#' must be one of `"two.sided"` (default), `"greater"` or `"less"`.
#' @param paired `TRUE` or `FALSE`. if `TRUE` a paired t-test is calculated.
#' @param var.equal `TRUE` or `FALSE`. Indicates whether to treat the two variances
#' as being equal. If `TRUE` then the pooled variance is used to estimate the
#' variance. If `FALSE` the Welch (or Satterthwaite) approximation to the degrees
#' of freedom is used.
#' @param mu A numeric for the value of the mean (or difference of means) assumed
#' under the null hypothesis.
#'
#' @return numeric
#'
#' @importFrom collapse fmean.default
#' @importFrom Rfast Var
#' @importFrom stats pt
#'
#' @export
#'
#' @examples
#' #----------------------------------------------------------------------------
#' # t_test_fast() examples
#' #----------------------------------------------------------------------------
#' library(bkstat)
#'
#' # Two sample test
#' t_test_fast(1:10, 7:20)
#' t_test_fast(1:10, c(7:20, 200))
#'
t_test_fast <- function(
    x,
    y = NULL,
    alternative = "two.sided",
    paired = FALSE,
    var.equal = FALSE,
    mu = 0
) {
  if(paired) { # if provided two samples of paired data
    x <- y - x
    y <- NULL
  }
  if(is.null(y)) { # test stat for one sample of data
    nx <- length(x)
    mx <- fmean.default(x)
    vx <- Var(x)
    df <- nx - 1L
    stderr <- sqrt(vx / nx)
    tstat <- (mx - mu) / stderr
  } else { # test stat for two samples of independent data
    nx <- length(x)
    mx <- fmean.default(x)
    vx <- Var(x)
    ny <- length(y)
    my <- fmean.default(y)
    vy <- Var(y)

    if(var.equal) {
      df <- nx + ny - 2L
      v <- 0L
      if(nx > 1L) {v <- v + (nx - 1L) * vx}
      if(ny > 1L) {v <- v + (ny - 1L) * vy}
      v <- v / df
      stderr <- sqrt(v * (1L / nx + 1L / ny))
    }
    else {
      stderrx <- sqrt(vx / nx)
      stderry <- sqrt(vy / ny)
      stderr <- sqrt(stderrx^2L + stderry^2L)
      df <- stderr^4L / (stderrx^4L / (nx - 1L) + stderry^4L / (ny - 1L))
    }

    tstat <- (my - mx - mu) / stderr
  }

  # Return pvalue
  if(alternative == "two.sided") {
    2L * pt(-abs(tstat), df)
  }
  else if(alternative == "greater") {
    pt(tstat, df, lower.tail = FALSE)
  }
  else {
    pt(tstat, df)
  }
}
