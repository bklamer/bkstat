#' Odds of Pathogenicity
#'
#' The strength of evidence for a functional assay can be determined by the odds
#' of pathogenicity (OddsPath) \insertCite{tavtigian_2018}{bkstat}.
#'
#' Following \insertCite{brnich_2019;textual}{bkstat},
#' we can calculate an optimistic OddsPath based on a perfect binary classifier
#' of the control variants. let
#'
#' \deqn{p_1 = \frac{\text{Number of pathogenic variants}}{\text{Total number variants}}}
#'
#' be the proportion of pathogenic variants (prior probability),
#'
#' \deqn{p_{2,\text{benign}} = \frac{1}{\text{Number of benign variants} + 1}}
#'
#' be the posterior probability for pathogenicity of a variant that has a benign
#' readout, and
#'
#' \deqn{p_{2,\text{pathogenic}} = \frac{\text{Number of pathogenic variants}}{\text{Number of pathogenic variants} + 1}}
#'
#' be the posterior probability for pathogenicity of a variant that has a pathogenic
#' readout, then
#'
#' \deqn{
#'   \begin{aligned}
#'     \text{OddsPath}_{\text{benign}} &= \frac{p_{2,\text{benign}}(1 - p_1)}{p_1(1-p_{2,\text{benign}})} \\
#'     \text{OddsPath}_{\text{pathogenic}} &= \frac{p_{2,\text{pathogenic}}(1 - p_1)}{p_1(1-p_{2,\text{pathogenic}})}
#'   \end{aligned}
#' }
#'
#' @param benign An integer for the number control assay benign samples.
#' @param pathogenic An integer for the number of control assay pathogenic samples.
#' @param round An integer for the number of decimal places.
#'
#' @return list
#'
#' @references
#' \insertRef{tavtigian_2018}{bkstat}
#'
#' \insertRef{brnich_2019}{bkstat}
#'
#' @export
#'
#' @examples
#' #----------------------------------------------------------------------------
#' # oddspath() examples
#' #----------------------------------------------------------------------------
#' library(bkstat)
#'
#' data.frame(
#'   oddspath = c(
#'     "<0.0029", "<0.053", "<0.23", "<0.48", "0.48-2.1", ">2.1", ">4.3", ">18.7",
#'     ">350"
#'   ),
#'   evidence_strength = c(
#'     "BS3_very_strong", "BS3", "BS3_moderate", "BS3_supporting", "Indeterminate",
#'     "PS3_supporting", "PS3_moderate", "PS3", "PS3_very_strong"
#'   )
#' )
#'
#' oddspath(10, 10)
#' oddspath(30, 30)
#'
oddspath <- function(benign, pathogenic, round = 2) {
  total = benign + pathogenic
  p1 = pathogenic / total
  p2_benign = 1 / (benign + 1)
  p2_pathogenic = pathogenic / (pathogenic + 1)

  oddspath_benign <- (
    (p2_benign * (1 - p1)) /
      (p1 * (1 - p2_benign))
  ) |>
    round(digits = round)

  oddspath_pathogenic <- (
    (p2_pathogenic * (1 - p1)) /
      (p1 * (1 - p2_pathogenic))
  ) |>
    round(digits = round)

  data.frame(
    Type = c("Benign", "Pathogenic"),
    OddsPath = c(oddspath_benign, oddspath_pathogenic)
  )
}
