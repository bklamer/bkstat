#' Convert a generic `rms` object into a data.frame
#'
#' Converts a generic two-dimensional \code{rms} object into a data.frame.
#'
#' @param x An \code{rms} object.
#'
#' @return data.frame
#'
#' @export as_df.rms
#' @usage as_df.rms(x)
#'
#' @examples
#' #----------------------------------------------------------------------------
#' # as_df.rms() examples
#' #----------------------------------------------------------------------------
#' library(bkstat)
#' library(rms)
#'
#' # Simulate object
#' x1 <- rnorm(500)
#' x2 <- sample(0:1, 500, rep = TRUE)
#' y  <- ifelse(runif(500) <= plogis(x1 + x2), 1, 0)
#' data <- data.frame(x1,x2,y)
#' mod <- lrm(y ~ x1 + x2, data = data)
#' mod_anova <- anova(mod)
#'
#' # Convert object into a data.frame
#' as_df.rms(mod_anova)
#'
as_df.rms <- function(x) {
  class(x) <- "matrix"
  x <- data.frame(term = row.names(x), x, row.names = NULL)
  x
}
