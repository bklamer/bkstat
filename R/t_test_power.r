#' Power of a t-test based on fold change
#'
#' Power of a t-test based on fold change. This method is based on the following
#' steps:
#' 1) State the hypotheses in terms of the ratio of the geomtric means of the log
#' transformed data (fold change).
#' 2) Select values for other relevant power analysis parameters. The coefficient(s)
#' of variation and the correlation between paired data are based on the original
#' untransformed data.
#' 3) Repeatedly simulate the log-scale data.
#' 4) Calculate the probability of correctly rejecting the null hypothesis over
#' all simulations.
#'
#' For samples \eqn{X} and \eqn{Y}, the fold change (FC) is defined as
#' \deqn{FC = \frac{Mean(Y)}{Mean(X)}}
#' The coefficient of variation (CV) for sample \eqn{X} is defined as
#' \deqn{CV = \frac{SD(X)}{Mean(X)}}
#'
#' If we take the natural log of the data, the difference of the means corresponds
#' to the log of the ratio of the geometric means. Thus, a t-test on the logged
#' data will provide information about the hypotheses for fold change.
#'
#' The relationship between sample statistics for the original data (\eqn{X}) and
#' the natural logged data (\eqn{\ln{X}}) are
#'
#' \deqn{Mean(X) = e^{Mean(\ln{X}) + \frac{Var(\ln{X})}{2}}}
#' \deqn{Var(X) = Mean(X)^2 \left( e^{Var(\ln{X})} - 1 \right)}
#' \deqn{
#' \begin{aligned}
#' CV(X) &= \frac{\sqrt{Mean(X)^2 \left( e^{Var(\ln{X})} - 1 \right)}}{Mean(X)} \\
#'       &= \sqrt{e^{Var(\ln{X})} - 1}
#' \end{aligned}
#' }
#' which results in
#' \deqn{Var(\ln{X}) = \ln(CV(X)^2 + 1)}
#' \deqn{Mean(\ln{X}) = \ln \left( \frac{Mean(X)}{\sqrt{CV(X)^2 + 1}} \right)}
#' \deqn{Cor(\ln{X}, \ln{Y}) = \frac{\ln \left( Cor(X, Y)CV(X)CV(Y) + 1 \right)}{SD(\ln{X})SD(\ln{Y})}}
#'
#' To better understand the properties of correlation and variances, remember that
#'
#' \deqn{Var(X - Y) = Var(X) + Var(Y) - 2Cov(X, Y)}
#'
#' The standard deviation of the differences is the square root of this. If we
#' expand this equation a bit further it turns into
#'
#' \deqn{SD(X - Y) = \sqrt{Var(X) + Var(Y) - 2Cor(X, Y)SD(X)SD(Y)}}
#'
#' Thus, the standard deviation of the differences gets smaller the more positive
#' the correlation and conversely gets larger the more negative the correlation.
#' For the special case where the two samples are uncorrelated and each has the
#' same variance, it follows that
#'
#' \deqn{Var(X-Y) = \sigma^2 + \sigma^2}
#' \deqn{SD(X-Y) = \sqrt{2}\sigma}
#'
#' This function provides the pre-study power of a t-test for four scenarios:
#'
#' 1. Independent two sample t-test
#'     - `fc_true`: The FC for sample X and sample Y
#'     - `cv1` and `cv2`: The CVs for sample X and sample Y
#'     - Allows different sample sizes
#'     - Allows different CV per sample
#' 2. Paired two sample t-test
#'     - `fc_true`: The within-sample FC for sample X and sample Y
#'     - `cv1` and `cv2`: The CVs for sample X and sample Y
#'     - `cor`: The correlation between sample X and Y
#'     - Allows different CV per sample. Hence the SD of the differences is a
#'       function of `cv1`, `cv2`, and `cor`.
#' 3. Paired difference one sample t-test
#'     - `fc_true`: The within-sample FC for sample X and sample Y
#'     - `cv1`: The CV of the differences
#'     - Hence the correlation between samples is a function of `cv1`
#' 4. Population difference one sample t-test
#'     - `fc_true`: The FC for sample X relative to the population
#'       \eqn{\frac{Mean(X)}{\mu}}
#'     - `cv1`: The CV for sample X
#'
#' @param n1 An integer for the sample size of sample 1.
#' @param n2 An integer for the sample size of sample 2.
#' @param fc_null A numeric for the fold change assumed in the null hypothesis.
#' e.g. `fc_null = 1` assumes that there is no difference in means between
#' sample 1 and sample 2.
#' @param fc_true A numeric for the assumed true population fold change between
#' sample 1 and sample 2 (mean sample 2 / mean sample 1). e.g. `fc_true = 2`
#' assumes that the mean of sample 2 is 2 times larger than the mean of sample 1.
#' @param cv1 A numeric for the assumed true coefficient of variation of sample 1.
#' @param cv2 A numeric for the assumed true coefficient of variation of sample 2.
#' @param cor A numeric for the assumed true correlation between sample 1 and
#' sample 2. If `cor == 0`, an independent two sample t.test is used. If `cor != 0`,
#' a paired two sample t.test is used.
#' @param alpha A numeric for probability of a false positive test result (type
#' 1 error).
#' @param iters An integer for the number of simulation iterations.
#' @param alternative A character string specifying the alternative hypothesis,
#' must be one of `"two.sided"` (default), `"greater"` or `"less"`.
#' @param var.equal `TRUE` or `FALSE`. Indicates whether to treat the two variances
#' as being equal. If `TRUE` then the pooled variance is used to estimate the
#' variance. If `FALSE` the Welch (or Satterthwaite) approximation to the degrees
#' of freedom is used.
#'
#' @return numeric
#'
#' @importFrom bkmisc check_positive check_numeric check_probability check_correlation
#'                    check_string check_logical
#' @importFrom mvnfast rmvn
#'
#' @references
#' \insertRef{julious_2004}{bkstat}
#'
#' \insertRef{cohen_1988}{bkstat}
#'
#' @export
#'
#' @examples
#' #----------------------------------------------------------------------------
#' # t_test_power() examples
#' #----------------------------------------------------------------------------
#' library(bkstat)
#'
#' # Independent two sample test
#' t_test_power(
#'   n1 = 30,
#'   n2 = 30,
#'   fc_null = 1,
#'   fc_true = 1.3,
#'   cv1 = 0.35,
#'   cv2 = 0.35,
#'   cor = 0,
#'   iters = 1000
#' )
#'
#' # Paired two sample test
#' t_test_power(
#'   n1 = 30,
#'   n2 = 30,
#'   fc_null = 1,
#'   fc_true = 1.3,
#'   cv1 = 0.35,
#'   cv2 = 0.35,
#'   cor = 0.5,
#'   iters = 1000
#' )
#'
#' # Paired difference one sample test
#' t_test_power(
#'   n1 = 30,
#'   fc_null = 1,
#'   fc_true = 1.3,
#'   cv1 = 0.35,
#'   iters = 1000
#' )
#'
#' # Population difference one sample test
#' t_test_power(
#'   n1 = 30,
#'   fc_null = 1,
#'   fc_true = 1.3,
#'   cv1 = 0.35,
#'   iters = 1000
#' )
#'
#' # Example figure for independent two sample hypothesis
#' grid <- expand.grid(
#'   n = seq(from = 10, to = 30, by = 5),
#'   fc_true = seq(from = 1.25, to = 2, by = 0.25),
#'   cv = seq(from = 0.3, to = 0.5, by = 0.1)
#' )
#' grid_list <- asplit(grid, 1)
#'
#' res <- lapply(grid_list, function(x) {
#'   t_test_power(
#'     n1 = x[["n"]],
#'     n2 = x[["n"]],
#'     fc_null = 1,
#'     fc_true = x[["fc_true"]],
#'     cv1 = x[["cv"]],
#'     cv2 = x[["cv"]],
#'     cor = 0,
#'     iters = 3000
#'   )
#' }) |>
#'   unlist() |>
#'   dplyr::bind_cols(grid) |>
#'   dplyr::rename(power = ...1) |>
#'   dplyr::relocate(power, .after = "cv") |>
#'   dplyr::mutate(
#'     fc_true = paste0("Population FC=", fc_true),
#'     cv = paste0("Population CV=", cv)
#'   )
#'
#' library(ggplot2)
#' ggplot(res, aes(x = n, y = power)) +
#'   facet_grid(fc_true ~ cv) +
#'   geom_line() +
#'   geom_point(size = 0.5) +
#'   geom_hline(yintercept = 0.8, alpha = 0.4) +
#'   labs(
#'     x = "Sample Size",
#'     y = "Power (probability of rejecting the null hypothesis)",
#'     caption = "Power analysis for an independent two-sample t-test based on fold change"
#'   )
#'
t_test_power <- function(
    n1,
    n2 = NULL,
    fc_null = 1,
    fc_true,
    cv1,
    cv2 = NULL,
    cor = 0,
    alpha = 0.05,
    iters = 10000,
    alternative = "two.sided",
    var.equal = FALSE
) {
  #-----------------------------------------------------------------------------
  # Check arguments
  #-----------------------------------------------------------------------------
  check_positive(n1)
  if(!is.null(n2)) {check_positive(n2)}
  check_numeric(fc_null)
  check_numeric(fc_true)
  check_numeric(cv1)
  if(!is.null(cv2)) {check_numeric(cv2)}
  check_correlation(cor)
  check_probability(alpha)
  check_positive(iters)
  check_string(alternative)
  check_logical(var.equal)

  if((is.null(n2) & !is.null(cv2)) | (!is.null(n2) & is.null(cv2))) {
    stop("Arguments 'n2' and 'cv2' must both be NULL (one sample test) or both numeric (two sample test)")
  }
  one_sample <- is.null(n2)

  #-----------------------------------------------------------------------------
  # Simulate data
  #-----------------------------------------------------------------------------
  log_fc_null <- log(fc_null)
  log_fc_true <- log(fc_true)

  if(one_sample) {
    sigma <- sqrt(log(cv1^2L + 1L))

    sim_p <- vapply(
      X = seq_len(iters),
      FUN = function(x) {
        sim_data <- rnorm(n = n1, mean = log_fc_true, sd = sigma)

        t_test_fast(
          x = sim_data,
          alternative = alternative,
          mu = log_fc_null
        )
      },
      FUN.VALUE = numeric(1L),
      USE.NAMES = FALSE
    )
  } else {
    paired <- cor != 0L
    diff_ss <- n1 != n2
    if(diff_ss & paired) {
      stop("Paired data must have the same sample size for both samples.")
    }
    maxn <- max(n1, n2)
    n1_smaller <- n1 < n2

    sigma1 <- sqrt(log(cv1^2L + 1L))
    sigma2 <- sqrt(log(cv2^2L + 1L))
    log_cor <- log(cor*cv1*cv2 + 1L) / (sigma1*sigma2)
    cormat <- matrix(c(1L, log_cor, log_cor, 1L), 2L, 2L)
    varmat <- matrix(c(sigma1^2L, sigma1*sigma2, sigma1*sigma2, sigma2^2L), 2L, 2L)
    covmat <- cormat * varmat
    # Alternative calculation of covariances
    #covar <- sqrt(sigma1^2L * sigma2^2L) / (1L / log_cor)
    #covmat <- matrix(c(sigma1^2L, covar, covar, sigma2^2L), 2L, 2L)
    # Check if the covariance matrix is positive semidefinite
    #stopifnot(all(eigen(covmat)$values >= 0L))

    A <- matrix(NA_real_, maxn, 2L)
    sim_p <- vapply(
      X = seq_len(iters),
      FUN = function(x) {
        sim_data <- sim_log_two_sample(
          maxn = maxn,
          log_fc_true = log_fc_true,
          covmat = covmat,
          diff_ss = diff_ss,
          n1_smaller = n1_smaller,
          n1 = n1,
          n2 = n2,
          A = A
        )

        t_test_fast(
          x = sim_data[[1L]],
          y = sim_data[[2L]],
          alternative = alternative,
          var.equal = var.equal,
          paired = paired,
          mu = log_fc_null
        )
      },
      FUN.VALUE = numeric(1L),
      USE.NAMES = FALSE
    )
  }

  #-----------------------------------------------------------------------------
  # Return power
  #-----------------------------------------------------------------------------
  sum(sim_p <= alpha) / iters
}


# Simulate data for fold change two sample power analysis.
sim_log_two_sample <- function(
    maxn,
    log_fc_true,
    covmat,
    diff_ss,
    n1_smaller,
    n1,
    n2,
    A
) {
  # Simulate data and assign to preallocated matrix A
  rmvn(
    n = maxn,
    mu = c(0L, log_fc_true),
    sigma = covmat,
    A = A
  )

  # Return
  if(diff_ss) { # two sample data with unequal sample sizes
    if(n1_smaller) {
      list(x = A[,1L][seq_len(n1)], y = A[,2L])
    } else {
      list(x = A[,1L], y = A[,2L][seq_len(n2)])
    }
  } else { # two sample data with equal sample size or paired data
    list(x = A[,1L], y = A[,2L])
  }
}
