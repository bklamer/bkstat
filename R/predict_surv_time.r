#' Predicted median survival time
#'
#' Get the survival time at a given probability of survival. A convenience
#' wrapper for \code{survival::\link[survival]{quantile.survfit}}.
#'
#' If the survival curve or its confidence limits do not cross the probability of
#' interest (i.e. they extend out to infinity time without crossing the chosen
#' probability), then `NA` is returned for the time estimate.
#'
#' The probabilities are defined using the cumulative distribution function
#' F(t) = 1-S(t). For example, `prob = 0.9` will return the survival time at the
#' point of 10% probability of surviving.
#'
#' @param object An object of either
#' - \code{survival::\link[survival]{coxph}}, or
#' - Survival formula using \code{survival::\link[survival]{Surv}}
#' @param df A data frame in which to interpret the variables in `object`.
#' - For `object = survival::coxph`, may be `NULL` or the usual `newdata` style argument.
#' - For `object = formula`, must be the data frame used for fitting in
#' \code{survival::\link[survival]{survfit}}.
#' @param probs A numeric vector of probabilities. This argument applies to the
#' cumulative distribution function F(t) = 1-S(t). For example, `prob = 0.9` will
#' return the survival time at the point of 10% probability of surviving.
#' @param conf.level The level for a two-sided confidence interval on the survival
#' time(s). Default is 0.95.
#' @param scale A numeric value to rescale the survival time, e.g., if the input
#' data to survfit were in days, scale = 365.25 would scale the output to years.
#' @param ... Optional arguments passed to \code{survival::\link[survival]{survfit}}.
#'
#' @return data.frame
#'
#' @importFrom survival survfit
#' @importFrom bkmisc check_data_frame check_probability pretty_print
#' @importFrom bkdat add_column
#' @importFrom dplyr arrange across any_of mutate where relocate
#' @importFrom tidyr gather separate_longer_delim separate_wider_delim pivot_wider unnest
#'
#' @export
#'
#' @examples
#' #----------------------------------------------------------------------------
#' # surv_time() examples.
#' #----------------------------------------------------------------------------
#' library(survival)
#' library(bkstat)
#'
#' bkstat::predict_surv_time(
#'   object = coxph(Surv(time, status) ~ sex + ph.ecog, data = lung),
#'   df = data.frame(expand.grid(sex = 1:2, ph.ecog = 0:3)),
#'   probs = c(0.5, 0.9)
#' )
#' bkstat::predict_surv_time(
#'   Surv(time, status) ~ sex + ph.ecog,
#'   df = lung,
#'   probs = c(0.5, 0.9)
#' )
#' bkstat::predict_surv_time(
#'   object = coxph(Surv(time, status) ~ 1, lung),
#'   probs = c(0.5, 0.9)
#' )
#' bkstat::predict_surv_time(
#'   Surv(time, status) ~ 1,
#'   df = lung,
#'   probs = c(0.5, 0.9)
#' )
#'
predict_surv_time <- function(
  object,
  df = NULL,
  probs = 0.5,
  conf.level = 0.95,
  scale = 1,
  ...
){
  #-----------------------------------------------------------------------------
  # Check arguments
  #-----------------------------------------------------------------------------
  if(!inherits(object, c('formula', 'coxph', 'survreg', 'aareg'))) {
    stop(
      "Check the 'object' argument in function bkstat::predict_surv_time(). 'object' has class ", pretty_print(class(arg)), ", but needs to be class 'formula' or a survival model object.",
      call. = FALSE
    )
  }
  if(!is.null(df)) {check_data_frame(df)}
  check_probability(conf.level)
  check_probability(probs)

  #-----------------------------------------------------------------------------
  # Function to convert survival::quantile.survfit to a data.frame.
  #-----------------------------------------------------------------------------
  quantile_survfit_coxph <- function(x, df) {
    # if formula = Surv() ~ 1, then quantile returns output in a different format.
    # So this will transform to more generalized format.
    if(inherits(x$quantile, "numeric")) {
      x$quantile <- t(x$quantile)
      x$lower <- t(x$lower)
      x$upper <- t(x$upper)
    }

    times <- as.data.frame(x$quantile)
    lower <- as.data.frame(x$lower)
    upper <- as.data.frame(x$upper)

    times <- tidyr::gather(times, key = "prob", value = "time")
    lower <- tidyr::gather(lower, key = "prob", value = "lower_ci")
    upper <- tidyr::gather(upper, key = "prob", value = "upper_ci")

    # In quantile.survfit, the probs argument corresponds to the cumulative
    # distribution of F(t) = 1 - S(t). So take 1 - prob below to make in terms
    # of the survival probability as the user would expect.
    out <- cbind(
      times,
      lower["lower_ci"],
      upper["upper_ci"],
      stringsAsFactors = FALSE,
      row.names = NULL
    ) |>
      dplyr::mutate(
        prob = 1 - (as.numeric(prob) / 100)
      )

    if(is.null(df)) { # Overall prediction
      out |>
        dplyr::arrange(prob) |>
        as.data.frame()
    } else { # Conditional predictions
      cbind(df, out, row.names = NULL) |>
        dplyr::arrange(prob, dplyr::across(dplyr::any_of(names(df)))) |>
        as.data.frame()
    }
  }

  quantile_survfit_formula <- function(x) {
    # if formula = Surv() ~ 1, then quantile returns output in a different format.
    # So this will transform to more generalized format.
    has_vars <- inherits(x$quantile, "matrix")
    if(!has_vars) {
      x$quantile <- t(x$quantile)
      x$lower <- t(x$lower)
      x$upper <- t(x$upper)
    }

    times <- as.data.frame(x$quantile)
    lower <- as.data.frame(x$lower)
    upper <- as.data.frame(x$upper)

    prob_cols <- names(times)

    if(has_vars) {
      times <- times |>
        bkdat::add_column(x = list(strata = rownames(times)), before = 1) |>
        tidyr::separate_longer_delim(cols = "strata", delim = ",") |>
        tidyr::separate_wider_delim(cols = "strata", delim = "=", names = c("variable", "value")) |>
        dplyr::mutate(variable = trimws(variable), value = trimws(value))
      names <- unique(times$variable)
      # The pivot needs to account for situations when times have missing values.
      # It results in list-cols, so unnesting is required.
      times <- times |>
        tidyr::pivot_wider(names_from = "variable", values_from = "value", values_fn = list) |>
        tidyr::unnest(cols = dplyr::where(is.list)) |>
        dplyr::relocate(dplyr::all_of(names), .before = 1) |>
        dplyr::arrange(dplyr::across(dplyr::all_of(names))) |>
        as.data.frame()

      lower <- lower |>
        bkdat::add_column(x = list(strata = rownames(lower)), before = 1) |>
        tidyr::separate_longer_delim(cols = "strata", delim = ",") |>
        tidyr::separate_wider_delim(cols = "strata", delim = "=", names = c("variable", "value")) |>
        dplyr::mutate(variable = trimws(variable), value = trimws(value))
      names <- unique(lower$variable)
      # The pivot needs to account for situations when times have missing values.
      # It results in list-cols, so unnesting is required.
      lower <- lower |>
        tidyr::pivot_wider(names_from = "variable", values_from = "value", values_fn = list) |>
        tidyr::unnest(cols = dplyr::where(is.list)) |>
        dplyr::relocate(dplyr::all_of(names), .before = 1) |>
        dplyr::arrange(dplyr::across(dplyr::all_of(names))) |>
        as.data.frame()

      upper <- upper |>
        bkdat::add_column(x = list(strata = rownames(upper)), before = 1) |>
        tidyr::separate_longer_delim(cols = "strata", delim = ",") |>
        tidyr::separate_wider_delim(cols = "strata", delim = "=", names = c("variable", "value")) |>
        dplyr::mutate(variable = trimws(variable), value = trimws(value))
      names <- unique(upper$variable)
      # The pivot needs to account for situations when times have missing values.
      # It results in list-cols, so unnesting is required.
      upper <- upper |>
        tidyr::pivot_wider(names_from = "variable", values_from = "value", values_fn = list) |>
        tidyr::unnest(cols = dplyr::where(is.list)) |>
        dplyr::relocate(dplyr::all_of(names), .before = 1) |>
        dplyr::arrange(dplyr::across(dplyr::all_of(names))) |>
        as.data.frame()
    }

    times <- tidyr::gather(times, key = "prob", value = "time", dplyr::all_of(prob_cols))
    lower <- tidyr::gather(lower, key = "prob", value = "lower_ci", dplyr::all_of(prob_cols))
    upper <- tidyr::gather(upper, key = "prob", value = "upper_ci", dplyr::all_of(prob_cols))

    # In quantile.survfit, the probs argument corresponds to the cumulative
    # distribution of F(t) = 1 - S(t). So take 1 - prob below to make in terms
    # of the survival probability as the user would expect.
    join_cols <- names(times)[!names(times) %in% c("prob", "time")]
    times <- times |>
      dplyr::left_join(lower, by = c("prob", join_cols)) |>
      dplyr::left_join(upper, by = c("prob", join_cols)) |>
      dplyr::mutate(prob = 1 - (as.numeric(prob) / 100)) |>
      dplyr::arrange(prob, dplyr::across(dplyr::any_of(join_cols))) |>
      as.data.frame()

    times
  }

  #-----------------------------------------------------------------------------
  # Run quantile.survfit() and transform its output to a tidy format
  #-----------------------------------------------------------------------------
  if(inherits(object, c('coxph', 'survreg', 'aareg'))) {
    if(is.null(df)) {
      sf <- survfit(formula = object, conf.int = conf.level, ...)
    } else {
      sf <- survfit(formula = object, newdata = df, conf.int = conf.level, ...)
    }
  } else {
    sf <- survfit(formula = object, data = df, conf.int = conf.level, ...)
  }

  sfq <- quantile(sf, probs = probs, conf.int = TRUE, scale = scale)

  if(inherits(object, c('coxph', 'survreg', 'aareg'))) {
    out <- quantile_survfit_coxph(x = sfq, df = df)
  } else {
    out <- quantile_survfit_formula(x = sfq)
  }

  #-----------------------------------------------------------------------------
  # Return
  #-----------------------------------------------------------------------------
  out
}
