#' Intraclass correlation coefficient
#'
#' When every subject is evaluated by the same set of raters/methods, and they are the only set of raters/methods of interest, then ICC is calculated using the ICC(C,1) and ICC(A,1) methods as found in McGraw and Wong, 1996. For more details, see <https://www.stata.com/manuals13/ricc.pdf>.
#'
#' @param data A data.frame.
#' @param estimate A string of either "both", "agreement", or "consistency".
#' @param formula A formula object to be used with `lme4::lmer()`. If needed, include the interaction of 'id' and 'method' so ICC(A,1) can be calculated.
#' @param method A string of the method column name.
#' @param id A string of the id column name.
#' @param reference A string if the method name if a gold-standard.
#' @param iters An integer for the number of bootstrap iterations for calculating confidence intervals.
#'
#' @return a data.frame with columns: method1, method2, icc_type, icc, icc_sd, icc_lower, icc_upper
#'
#' @importFrom utils combn
#'
#' @export
#'
#' @examples
#' #----------------------------------------------------------------------------
#' # icc() examples
#' #----------------------------------------------------------------------------
#' library(bkstat)
#'
#' # icc(
#' #   data = data,
#' #   formula = as.formula("value ~ (1 | id) + (1 | method) + (1 | id:method)"),
#' #   method = "method",
#' #   id = "id,
#' #   reference = "Actual"
#' # )
#'
icc <- function(data, estimate = "both", formula, method, id, reference = NULL, iters = 1000) {
  # create a two column matrix of pairwise method combinations
  pair_mat <- t(combn(unique(data[[method]]), 2, simplify = TRUE))
  if(!is.null(reference)) {
    keep_row <- matrix(pair_mat == reference, ncol = 2)
    pair_mat <- pair_mat[rowSums(keep_row) > 0, ]
  }

  # 1. walk along pairwise combinations and get icc point estimate.
  # 2. use bootstrap for confidence interval estimate
  out <- data.frame(
    method1 = rep(NA_character_, nrow(pair_mat)),
    method2 = rep(NA_character_, nrow(pair_mat)),
    icc_type = rep(NA_character_, nrow(pair_mat)),
    icc = rep(NA_real_, nrow(pair_mat)),
    icc_sd = rep(NA_real_, nrow(pair_mat)),
    icc_lower = rep(NA_real_, nrow(pair_mat)),
    icc_upper = rep(NA_real_, nrow(pair_mat)),
    stringsAsFactors = FALSE
  )
  out_a <- out
  out_c <- out

  for(i in seq_len(nrow(pair_mat))) {
    mod <- lmer(
      formula,
      data = data[data[[method]] %in% pair_mat[i, ], ]
    )

    mod_sd <- as.data.frame(VarCorr(mod))
    sd_id <- mod_sd[mod_sd$grp == id, "sdcor"]
    sd_id_method <- if(any(":" %in% as.character(formula))) {
      mod_sd[grep(":", mod_sd$grp, fixed = TRUE), "sdcor"]
    } else {
      sd_id_method <- 0
    }
    sd_error <- mod_sd[mod_sd$grp == "Residual", "sdcor"]

    if(estimate == "both") {
      icc_a <- (sd_id^2 - (sd_id_method^2 / (2-1))) /
        (sd_id^2 + (sum(ranef(mod)$method^2) / (2-1)) + (sd_id_method^2 + sd_error^2))
      icc_c <- (sd_id^2 - (sd_id_method^2 / (2-1))) /
        (sd_id^2 + (sd_id_method^2 + sd_error^2))
    } else if(estimate == "agreement") {
      icc_a <- (sd_id^2 - (sd_id_method^2 / (2-1))) /
        (sd_id^2 + (sum(ranef(mod)$method^2) / (2-1)) + (sd_id_method^2 + sd_error^2))
    } else if(estimate == "consistency") {
      icc_c <- (sd_id^2 - (sd_id_method^2 / (2-1))) /
        (sd_id^2 + (sd_id_method^2 + sd_error^2))
    } else {
      stop("Check 'estimate' argument in bkstat::icc()")
    }

    icc_a_boot <- vector(mode = "list", length = iters)
    icc_c_boot <- vector(mode = "list", length = iters)
    for(j in seq_len(iters)) {
      id_boot <- sample(unique(data[[id]]), size = length(unique(data[[id]])), replace = TRUE)
      id_boot_fix <- make.unique(as.character(id_boot), sep = "_")
      data_boot <- data.frame(id = id_boot, id_fix = id_boot_fix, stringsAsFactors = FALSE)
      data_boot <- merge(data_boot, data, all.x = TRUE, by.x = "id")
      data_boot <- data_boot[-grep("^id$", names(data_boot))]
      names(data_boot)[grep("^id_fix$", names(data_boot))] <- id

      mod <- lmer(
        formula,
        data = data_boot[data_boot[[method]] %in% pair_mat[i, ], ]
      )

      mod_sd <- as.data.frame(VarCorr(mod))
      sd_id <- mod_sd[mod_sd$grp == id, "sdcor"]
      sd_id_method <- if(any(":" %in% as.character(formula))) {
        mod_sd[grep(":", mod_sd$grp, fixed = TRUE), "sdcor"]
      } else {
        sd_id_method <- 0
      }
      sd_error <- mod_sd[mod_sd$grp == "Residual", "sdcor"]

      if(estimate == "both") {
        icc_a_boot[[j]] <- (sd_id^2 - (sd_id_method^2 / (2-1))) /
          (sd_id^2 + (sum(ranef(mod)$method^2) / (2-1)) + (sd_id_method^2 + sd_error^2))
        icc_c_boot[[j]] <- (sd_id^2 - (sd_id_method^2 / (2-1))) /
          (sd_id^2 + (sd_id_method^2 + sd_error^2))
      } else if(estimate == "agreement") {
        icc_a_boot[[j]] <- (sd_id^2 - (sd_id_method^2 / (2-1))) /
          (sd_id^2 + (sum(ranef(mod)$method^2) / (2-1)) + (sd_id_method^2 + sd_error^2))
      } else if(estimate == "consistency") {
        icc_c_boot[[j]] <- (sd_id^2 - (sd_id_method^2 / (2-1))) /
          (sd_id^2 + (sd_id_method^2 + sd_error^2))
      } else {
        stop("Check 'estimate' argument in bkstat::icc()")
      }
    }

    if(estimate == "both") {
      icc_a_quant <- quantile(
        sort(unlist(icc_a_boot)),
        probs = c(0.025, 0.975),
        na.rm = FALSE,
        names = TRUE,
        type = 7
      )
      icc_a_sd <- sd(unlist(icc_a_boot))
      icc_a_lower <- 2 * icc_a - icc_a_quant[2]
      icc_a_upper <- 2 * icc_a - icc_a_quant[1]

      icc_c_quant <- quantile(
        sort(unlist(icc_c_boot)),
        probs = c(0.025, 0.975),
        na.rm = FALSE,
        names = TRUE,
        type = 7
      )
      icc_c_sd <- sd(unlist(icc_c_boot))
      icc_c_lower <- 2 * icc_c - icc_c_quant[2]
      icc_c_upper <- 2 * icc_c - icc_c_quant[1]

      out_a[i, "method1"] <- as.character(pair_mat[i, 1])
      out_a[i, "method2"] <- as.character(pair_mat[i, 2])
      out_a[i, "icc_type"] <- "agreement"
      out_a[i, "icc"] <- icc_a
      out_a[i, "icc_sd"] <- icc_a_sd
      out_a[i, "icc_lower"] <- icc_a_lower
      out_a[i, "icc_upper"] <- icc_a_upper

      out_c[i, "method1"] <- as.character(pair_mat[i, 1])
      out_c[i, "method2"] <- as.character(pair_mat[i, 2])
      out_c[i, "icc_type"] <- "consistency"
      out_c[i, "icc"] <- icc_c
      out_c[i, "icc_sd"] <- icc_c_sd
      out_c[i, "icc_lower"] <- icc_c_lower
      out_c[i, "icc_upper"] <- icc_c_upper
    } else if(estimate == "agreement") {
      icc_a_quant <- quantile(
        sort(unlist(icc_a_boot)),
        probs = c(0.025, 0.975),
        na.rm = FALSE,
        names = TRUE,
        type = 7
      )
      icc_a_sd <- sd(unlist(icc_a_boot))
      icc_a_lower <- 2 * icc_a - icc_a_quant[2]
      icc_a_upper <- 2 * icc_a - icc_a_quant[1]

      out_a[i, "method1"] <- as.character(pair_mat[i, 1])
      out_a[i, "method2"] <- as.character(pair_mat[i, 2])
      out_a[i, "icc_type"] <- "agreement"
      out_a[i, "icc"] <- icc_a
      out_a[i, "icc_sd"] <- icc_a_sd
      out_a[i, "icc_lower"] <- icc_a_lower
      out_a[i, "icc_upper"] <- icc_a_upper

      out_c <- NULL
    } else if(estimate == "consistency") {
      icc_c_quant <- quantile(
        sort(unlist(icc_c_boot)),
        probs = c(0.025, 0.975),
        na.rm = FALSE,
        names = TRUE,
        type = 7
      )
      icc_c_sd <- sd(unlist(icc_c_boot))
      icc_c_lower <- 2 * icc_c - icc_c_quant[2]
      icc_c_upper <- 2 * icc_c - icc_c_quant[1]

      out_c[i, "method1"] <- as.character(pair_mat[i, 1])
      out_c[i, "method2"] <- as.character(pair_mat[i, 2])
      out_c[i, "icc_type"] <- "consistency"
      out_c[i, "icc"] <- icc_c
      out_c[i, "icc_sd"] <- icc_c_sd
      out_c[i, "icc_lower"] <- icc_c_lower
      out_c[i, "icc_upper"] <- icc_c_upper

      out_a <- NULL
    }
  }

  out <- rbind(out_a, out_c)
  out
}
