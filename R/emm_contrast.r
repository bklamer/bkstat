#' emmeans contrast utility function
#'
#' Combines emmeans contrast results (from `contrast()`) and confidence interval results (from `confint()`) into a single `data.frame`.
#'
#' @param contrast Object from `emmeans::contrast()`
#' @param confint Object from `emmeans::confint()`
#'
#' @return data.frame
#'
#' @export
#'
#' @examples
#'
#' #----------------------------------------------------------------------------
#' # emm_contrast() examples
#' #----------------------------------------------------------------------------
#' library(bkstat)
#'
emm_contrast <- function(contrast, confint) {
  contrast_df <- as.data.frame(contrast)
  confint_df <- as.data.frame(confint)

  stopifnot(all(contrast_df$contrast == confint_df$contrast))

  contrast_df$p.value <- contrast_df$p.value

  idx_last2 <- (ncol(confint_df)-1):ncol(confint_df)
  cbind(contrast_df, confint_df[idx_last2])
}
