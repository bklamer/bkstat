#' Convert `rms::validate` into a data.frame
#'
#' Converts an \code{rms::\link[rms]{validate}} object into a data.frame.
#'
#' @param x An \code{rms::\link[rms]{validate}} object.
#' @param auc `TRUE` or `FALSE`. If `TRUE` (default) will convert Dxy values, when present, into AUC.
#' @param keep A character vector of statistics to keep.
#'
#' @return data.frame
#'
#' @importFrom bkdat add_row
#'
#' @seealso \code{rms::\link[rms]{validate}}
#'
#' @export as_df.validate
#' @usage as_df.validate(x, auc = TRUE, keep = NULL)
#'
#' @examples
#' #----------------------------------------------------------------------------
#' # as_df.validate() examples
#' #----------------------------------------------------------------------------
#' library(bkstat)
#' library(rms)
#'
#' n <- 1000    # define sample size
#' age <- rnorm(n, 50, 10)
#' blood.pressure <- rnorm(n, 120, 15)
#' cholesterol <- rnorm(n, 200, 25)
#' sex <- factor(sample(c('female','male'), n,TRUE))
#'
#' # Specify population model for log odds that Y=1
#' L <- .4*(sex=='male') + .045*(age-50) +
#'   (log(cholesterol - 10)-5.2)*(-2*(sex=='female') + 2*(sex=='male'))
#' # Simulate binary y to have Prob(y=1) = 1/[1+exp(-L)]
#' y <- ifelse(runif(n) < plogis(L), 1, 0)
#'
#' f <- rms::lrm(y ~ sex*rms::rcs(cholesterol)+rms::pol(age,2)+blood.pressure, x=TRUE, y=TRUE)
#'
#' # Validate full model fit
#' res <- rms::validate(f, B=10)
#' res
#'
#' # Convert object into a data.frame
#' as_df.validate(res)
#'
as_df.validate <- function(x, auc = TRUE, keep = NULL) {
  class(x) <- "matrix"
  x <- data.frame(statistic = row.names(x), x, row.names = NULL)

  if(auc & "Dxy" %in% x$statistic) {
    dxy <- x[x$statistic == "Dxy", ]
    x <- bkdat::add_row(
      data = x,
      x = list(
        statistic = "ROC-AUC",
        index.orig = dxy2auc(dxy$index.orig),
        training = dxy2auc(dxy$training),
        test = dxy2auc(dxy$test),
        optimism = dxy2auc(dxy$optimism) - 0.5,
        index.corrected = dxy2auc(dxy$index.corrected),
        n = dxy$n
      ),
      after = 1,
    )
  }

  if(!is.null(keep)) {
    x <- x[x$statistic %in% keep, ]
  }

  x
}
