#' Format p-values
#'
#' Print formatted p-values. Code re-exported from \code{scales::\link[scales]{pvalue}}.
#'
#' @param x A numeric vector of p-values.
#' @param accuracy A numeric to round to. Use (e.g.) `0.001` to show 3 decimal
#' places of precision. If `NULL`, uses a heuristic that should ensure breaks
#' have the minimum number of digits needed to show the difference between
#' adjacent values.
#' @param decimal.mark A string used to indicate the numeric decimal point.
#' @param prefix A character vector of length 3 giving the prefixes to put in
#' front of p-values. The default values are `c("<", "", ">")` if add_p is `FALSE`
#' and `c("p<", "p=", "p>")` if `TRUE`.
#' @param add_p TRUE or FALSE. If TRUE, adds "p" to prefix.
#'
#' @importFrom scales number
#'
#' @seealso \code{scales::\link[scales]{pvalue}}
#'
#' @export
pval <- function(
  x,
  accuracy = .001,
  decimal.mark = ".",
  prefix = NULL,
  add_p = FALSE
) {
  out <- number(x, accuracy, decimal.mark = decimal.mark)
  below <- number(accuracy, accuracy, decimal.mark = decimal.mark)
  above <- number(1 - accuracy, accuracy, decimal.mark = decimal.mark)

  if (is.null(prefix)) {
    if (add_p) {
      prefix <- c("p<", "p=", "p>")
    } else {
      prefix <- c("<", "", ">")
    }
  } else {
    if (!is.character(prefix) || length(prefix) != 3) {
      stop("`prefix` must be a length 3 character vector", call. = FALSE)
    }
  }

  out <- paste0(prefix[[2]], out)
  out[x < accuracy] <- paste0(prefix[[1]], below)
  out[x > 1 - accuracy] <- paste0(prefix[[3]], above)
  out[is.na(x)] <- NA
  names(out) <- names(x)

  out
}
