#' Get the survival time at a given probability of survival
#'
#' This function has been superseded by \code{bkstat::\link[bkstat]{predict_surv_time}}.
#' Get the survival time at a given probability of survival. A wrapper for `survival:::quantile.survfit()`.
#' If the survival curve or its confidence limits do not cross the probability
#' of interest (i.e. they extend out to infinity time without dropping below 0.5
#' probability), then `NA` is returned for the time estimate.
#'
#' @param df A data frame.
#' @param formula Either a survival formula: `survival::Surv(time, time2, event) ~ variable` or a survival model: `survival::coxph()`.
#' @param probs A vector of the survival probabilities of interest.
#' @param ... Optional arguments passed to `survival:::quantile.survfit()`.
#'
#' @return data.frame
#'
#' @importFrom survival survfit
#' @importFrom bkdat gather_
#' @importFrom bkmisc check_data_frame check_probability pretty_print
#'
#' @export
#'
#' @examples
#' #----------------------------------------------------------------------------
#' # surv_time() examples.
#' #----------------------------------------------------------------------------
#' library(survival)
#' library(bkstat)
#'
#' plot(survfit(formula = Surv(futime, death) ~ trt, data = myeloid))
#' surv_time(
#'   df = myeloid,
#'   formula = Surv(futime, death) ~ trt,
#'   probs = c(0.25, 0.5, 0.75)
#' )
#' surv_time(
#'   df = myeloid,
#'   formula = coxph(Surv(futime, death) ~ trt, data = myeloid),
#'   probs = c(0.25, 0.5, 0.75)
#' )
#' surv_time(
#'   df = myeloid,
#'   formula = Surv(futime, death) ~ 1,
#'   probs = c(0.25, 0.5, 0.75)
#' )
#'
surv_time <- function(df, formula, probs = 0.5, ...) {
  #-----------------------------------------------------------------------------
  # Check arguments
  #-----------------------------------------------------------------------------
  check_data_frame(df)
  check_probability(probs)
  if(!inherits(formula, c('formula', 'coxph', 'survreg', 'aareg'))) {
    stop(
      "Check the 'formula' argument in function bkstat::surv_time(). 'formula' has class ", pretty_print(class(formula)), ", but needs to be class 'formula' or a survival model object.",
      call. = FALSE
    )
  }

  #-----------------------------------------------------------------------------
  # Run quantile.survfit() and transform its output to a tidy format
  #-----------------------------------------------------------------------------
  survfit_quant <- quantile(survfit(formula, data = df), probs = probs, ...)

  # if formula = Surv() ~ 1, then strata is 'Overall' and quantile returns
  # output in a different format. So below will transform to more generalized format.
  strata <- rownames(survfit_quant$quantile)
  if(is.null(strata)) {
    strata <- "Overall"

    survfit_quant$quantile <- t(survfit_quant$quantile)
    survfit_quant$lower <- t(survfit_quant$lower)
    survfit_quant$upper <- t(survfit_quant$upper)
  }

  # continue with normal methods of generalizing the output
  times <- as.data.frame(survfit_quant$quantile)
  lower <- as.data.frame(survfit_quant$lower)
  upper <- as.data.frame(survfit_quant$upper)

  times <- gather_(times, key = "prob", value = "time", colnames(times))
  lower <- gather_(lower, key = "prob", value = "lower_ci", colnames(lower))
  upper <- gather_(upper, key = "prob", value = "upper_ci", colnames(upper))

  # In quantile.survfit, the probs argument corresponds to the cumulative
  # distribution of F(t) = 1 - S(t). So take 1 - prob below to make in terms
  # of the survival probability as the user would expect.
  times$prob <- 1 - as.numeric(times$prob) / 100
  lower$prob <- 1 - as.numeric(lower$prob) / 100
  upper$prob <- 1 - as.numeric(upper$prob) / 100

  out <- cbind(
    strata,
    times,
    lower["lower_ci"],
    upper["upper_ci"],
    stringsAsFactors = FALSE
  )

  #-----------------------------------------------------------------------------
  # Return
  #-----------------------------------------------------------------------------
  out
}
