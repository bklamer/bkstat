#' Convert `Hmisc::rcorr` into a data.frame
#'
#' Converts an \code{Hmisc::\link[Hmisc]{rcorr}} object into a data.frame.
#'
#' @param x An \code{Hmisc::\link[Hmisc]{rcorr}} object.
#'
#' @return data.frame
#'
#' @seealso \code{Hmisc::\link[Hmisc]{rcorr}}
#'
#' @export as_df.rcorr
#' @usage as_df.rcorr(x)
#'
#' @examples
#'
#' #----------------------------------------------------------------------------
#' # as_df.rcorr() examples
#' #----------------------------------------------------------------------------
#' library(bkstat)
#' library(Hmisc)
#'
#' df <- mtcars[, c(1,3,4,5,6,7)]
#' as_df.rcorr(Hmisc::rcorr(as.matrix(df)))
#'
as_df.rcorr <- function(x) {
  is_ut <- upper.tri(x$r)

  out <- data.frame(
    var1 = rownames(x$r)[col(x$r)[is_ut]],
    var2 = rownames(x$r)[row(x$r)[is_ut]],
    n = x$n[is_ut],
    correlation = x$r[is_ut],
    p_value = x$P[is_ut]
  )

  out
}
