#' Extract random-effects variances, standard deviation, and correlations from an LMM
#'
#' Calculates the random-effects variances, standard deviation, and correlations
#' from an `lme4::lmer` or `nlme::lme` object and returns a data.frame of results.
#'
#' @param x An `lme4::lmer` or `nlme::lme` model object.
#'
#' @return data.frame
#'
#' @seealso \code{nlme::\link[nlme]{VarCorr}},
#' \code{lme4::\link[lme4]{VarCorr}}
#'
#' @importFrom lme4 VarCorr
#' @importFrom nlme VarCorr
#' @importFrom dplyr filter select rename mutate
#'
#' @export
#'
#' @examples
#'
#' #----------------------------------------------------------------------------
#' # lmm_re_varcor() examples
#' #----------------------------------------------------------------------------
#' library(bkstat)
#'
#' data(Orthodont, package="nlme")
#' mod_lmer <- lme4::lmer(distance ~ age + (age|Subject), data = Orthodont)
#' mod_lme <- nlme::lme(fixed = distance ~ age, random = ~age|Subject, data = Orthodont)
#'
#' lmm_re_varcor(mod_lmer)
#' lmm_re_varcor(mod_lme)
#'
lmm_re_varcor <- function(x) {
  if(inherits(x, "lmerMod")) {
    lmm_re_varcor_df <- as.data.frame(VarCorr(x)) |>
      dplyr::filter(is.na(var2)) |>
      dplyr::mutate(
        Source = paste0(
          grp,
          ifelse(is.na(var1), "", paste0(":", var1))
        ),
        .before = 1
      ) |>
      dplyr::select(-grp, -var1, -var2) |>
      dplyr::rename(
        "Variance" = "vcov",
        "SD" = "sdcor"
      )

    # Return
    lmm_re_varcor_df
  } else if(inherits(x, "lme")) {
    needs_rename <- c("SD" = "StdDev", "Correlation" = "Corr")

    lmm_re_varcor_df <- data.frame(VarCorr(x)[]) |>
      dplyr::mutate(
        Source = rownames(data.frame(VarCorr(x)[])),
        .before = 1
      ) |>
      dplyr::rename(tidyr::any_of(needs_rename)) |>
      dplyr::mutate(
        Variance = as.numeric(Variance),
        SD = as.numeric(SD)
      )

    rownames(lmm_re_varcor_df) <- NULL

    # Return
    lmm_re_varcor_df
  } else {
    stop("Can't calculate the variance, SD, and correlations using 'x' argument of class: ", bkmisc::pretty_print(class(x)))
  }
}
