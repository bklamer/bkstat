#' Descriptive statistics
#'
#' Creates a data frame of descriptive statistics.
#'
#' @param df A data frame.
#' @param vars A character vector of variables to summarise.
#' @param groups A character vector of variables to group by.
#' @param funs Summary functions for variable types.
#' @param drop_levels TRUE or FALSE. If TRUE, drops unused factor levels.
#' @param digits An integer for the number of digits to keep for numeric variables.
#'
#' @return data.frame
#'
#' @seealso \code{\link{summary}}, \code{\link[Hmisc]{describe}}
#'
#' @importFrom stats median quantile sd
#' @importFrom bkmisc check_data_frame check_character check_name_in_df
#'                    type clean_list
#' @importFrom bkdat bind_fill_rows add_column arrange_ group_by nest_by
#'                   reset_row_names
#'
#' @export
#'
#' @examples
#' #----------------------------------------------------------------------------
#' # descript() examples.
#' #----------------------------------------------------------------------------
#' library(bkstat)
#'
#' descript(mtcars, groups = c("cyl"))
#'
#' df <- bkdat::as_df(
#' list(
#'   group1 = factor(c("a", "a", "a", "b", "b", "b", "c", "c", "c"), levels = c("a", "b", "c")),
#'   group2 = factor(c("x", "y", "x", "y", "x", "y", "y", "y", "y"), levels = c("x", "y", "z")),
#'   group3 = c("n", "m", "n", "m", "n", "m", "m", "m", "m"),
#'   character1 = letters[1:9],
#'   numeric1 = rnorm(9),
#'   integer1 = sample(1:100, size = 9),
#'   date1 = as.Date(
#'     c("2018-12-01", "2018-12-02", "2018-12-03", "2018-12-04", "2018-12-05", "2018-12-06",
#'     "2018-12-07", "2018-12-08", "2018-12-09")
#'   )
#' )
#' )
#' groups <- c("group1", "group2")
#' vars <- c("character1", "numeric1", "integer1", "date1")
#' descript(df, vars, groups, drop_levels = TRUE)
#'
descript <- function(
  df,
  vars = NULL,
  groups = NULL,
  funs = NULL,
  drop_levels = FALSE,
  digits = getOption("digits")
) {
  UseMethod("descript")
}

#' @export
#' @rdname descript
descript.data.frame <- function(
  df,
  vars = NULL,
  groups = NULL,
  funs = NULL,
  drop_levels = FALSE,
  digits = getOption("digits")
) {
  #-----------------------------------------------------------------------------
  # Check arguments
  #-----------------------------------------------------------------------------
  check_data_frame(df)

  if(!is.null(groups)) {
    check_character(groups)
    check_name_in_df(name = groups, df = df)

    df <- group_by(data = df, group_cols = groups)
    ret <- descript(
      df = df,
      vars = vars,
      funs = funs,
      drop_levels = drop_levels,
      digits = digits
    )
    return(ret)
  }

  if(is.null(vars)) {
    vars <- setdiff(names(df), groups)
  } else {
    check_character(vars)
    check_name_in_df(name = vars, df = df)
    vars <- setdiff(vars, groups)
  }

  if(is.null(funs)) {
    funs <- list(
      numeric = list(
        N = function(x) length(x),
        n_missing = function(x) sum(is.na(x) | is.null(x)),
        n_unique = function(x) length(unique(x[!is.na(x)])),
        mean = function(x) mean(x, na.rm = TRUE),
        sd = function(x) sd(x, na.rm = TRUE),
        cv = function(x) sd(x, na.rm = TRUE) / mean(x, na.rm = TRUE),
        min = function(x) min(x, na.rm = TRUE),
        p25 = function(x) quantile(x, probs = 0.25, names = FALSE, na.rm = TRUE),
        p50 = function(x) median(x, na.rm = TRUE),
        p75 = function(x) quantile(x, probs = 0.75, names = FALSE, na.rm = TRUE),
        max = function(x) max(x, na.rm = TRUE)
      ),
      categorical = list(
        level1 = list(
          N = function(x) length(x),
          n_missing = function(x) sum(is.na(x) | is.null(x)),
          n_unique = function(x) length(unique(x[!is.na(x)]))
        ),
        # So this is more complicated than needed, but returns nice results
        # and can handle factors.
        level2 = list(
          N = function(x) {as.data.frame(table(x, useNA = "ifany"))$Freq},
          level = function(x) {as.data.frame(table(x, useNA = "ifany"))[[1]]}, # Returns NULL if used name rather than position?
          proportion = function(x) {
            tab_df <- as.data.frame(table(x, useNA = "ifany"))
            names(tab_df) <- c("level", "N")
            if(anyNA(tab_df$level)) {
              idx_na <- is.na(tab_df$level)
              tab_df$proportion <- NA
              tab_df[!idx_na, ]$proportion <- tab_df[!idx_na, ]$N / (length(x) - sum(is.na(x) | is.null(x)))
              tab_df[idx_na, ]$proportion <- tab_df[idx_na, ]$N / length(x)
            } else {
              tab_df$proportion <- tab_df$N / length(x)
            }
            tab_df$proportion
          }
        )
      ),
      date = list(
        N = function(x) length(x),
        n_missing = function(x) sum(is.na(x) | is.null(x)),
        n_unique = function(x) length(unique(x[!is.na(x)])),
        min_date = function(x) as.character(min(x, na.rm = TRUE)),
        p25_date = function(x) as.character(quantile(x, probs = 0.25, names = FALSE, na.rm = TRUE, type = 1)),
        p50_date = function(x) as.character(median(x, na.rm = TRUE)),
        p75_date = function(x) as.character(quantile(x, probs = 0.75, names = FALSE, na.rm = TRUE, type = 1)),
        max_date = function(x) as.character(max(x, na.rm = TRUE))
      )
    )
  }

  df <- df[vars]

  #-----------------------------------------------------------------------------
  # The '1st level' summary function
  # - categorical variables also need 'second level' summaries for each unique level
  # - numeric variables are fully defined by the '1st level' summary
  # - date variables are fully defined by the '1st level' summary
  #-----------------------------------------------------------------------------
  level1 <- function(df) {
    # helpers...
    numeric_summary <- function(x) {
      lapply(funs$numeric, function(y) {y(x)})
    }
    character_summary <- function(x) {
      lapply(funs$categorical$level1, function(y) {y(x)})
    }
    factor_summary <- function(x) {
      lapply(funs$categorical$level1, function(y) {y(x)})
    }
    date_summary <- function(x) {
      lapply(funs$date, function(y) {y(x)})
    }

    summary <- function(x) {
      switch(
        EXPR = type(x),
        numeric = numeric_summary(x),
        character = character_summary(x),
        factor = factor_summary(x),
        date = date_summary(x)
      )
    }

    # apply helpers...
    summary_list <- lapply(df, summary)

    # format return
    res <- lapply(summary_list, data.frame)
    res <- bind_fill_rows(res)
    res <- add_column(data = res, x = list(variable = vars), before = 1)
    res$delete_me_ordering <- 1

    # return
    res
  }

  res1 <- level1(df)

  #-----------------------------------------------------------------------------
  # The '2nd level' summary function
  # - summarizes the unique levels of categorical variables
  #-----------------------------------------------------------------------------
  level2 <- function(df) {
    # helpers...
    character_summary <- function(x) {
      lapply(funs$categorical$level2, function(y) {y(x)})
    }
    factor_summary <- function(x) {
      lapply(funs$categorical$level2, function(y) {y(x)})
    }

    summary <- function(x) {
      switch(
        EXPR = type(x),
        numeric = NULL,
        character = character_summary(x),
        factor = factor_summary(x),
        date = NULL
      )
    }

    # apply helpers...
    summary_list <- lapply(df, summary)

    # format return
    summary_list <- clean_list(summary_list)
    summary_df <- lapply(summary_list, data.frame)
    ## add variable column
    var_names <- names(summary_df)
    summary_df <- lapply(var_names, function(x) {
      tmp <- summary_df[[x]]
      tmp <- add_column(tmp, list(variable = x), before = 1)
      tmp$delete_me_ordering <- seq_len(nrow(summary_df[[x]])) + 1
      tmp
    })
    names(summary_df) <- var_names
    ## combine into a dataframe
    res <- do.call(rbind, summary_df)
    res <- reset_row_names(res)
    res$level <- as.character(res$level)

    # return
    res
  }

  res2 <- level2(df)

  #-----------------------------------------------------------------------------
  # combine 1st and 2nd level summaries
  #-----------------------------------------------------------------------------
  res <- bind_fill_rows(x = list(res2, res1))
  res <- arrange_(res, x = c("delete_me_ordering"))
  res <- res[order(match(res$variable, vars)), ]

  column_order <- unique(
    c(
      "group",
      "variable",
      names(funs$categorical$level2),
      names(funs$categorical$level1),
      names(funs$numeric),
      names(funs$date)
    )
  )
  order_idx <- match(names(res), column_order)
  order <- column_order[order_idx[!is.na(order_idx)]]

  #-----------------------------------------------------------------------------
  # Return
  #-----------------------------------------------------------------------------
  res[order]
}

#' @export
#' @rdname descript
descript.group_df <- function(
  df,
  vars = NULL,
  groups = NULL,
  funs = NULL,
  drop_levels = FALSE,
  digits = getOption("digits")
) {
  #-----------------------------------------------------------------------------
  # Check arguments
  #-----------------------------------------------------------------------------
  groups <- attr(df, "group_cols")
  grid <- nest_by(data = df, group_cols = groups)

  #-----------------------------------------------------------------------------
  # Create summaries by group and put them in a data frame.
  #-----------------------------------------------------------------------------
  list <- vector(mode = "list", length = nrow(grid))
  for(i in seq_len(nrow(grid))) {
    grid$data[[i]] <- descript(
      df = grid$data[[i]],
      vars = vars,
      funs = funs,
      drop_levels = drop_levels,
      digits = digits
    )
  }

  #-----------------------------------------------------------------------------
  # Return
  #-----------------------------------------------------------------------------
  bkdat::unnest(grid, cols = "data")
}
