#' Standardize a numeric vector
#'
#' Standardize a numeric vector. Defaults to centering on the mean and scaling
#' on the standard deviation.
#'
#' @param x A numeric vector.
#' @param center A function or numeric constant.
#' @param scale A function or numeric constant.
#'
#' @return numeric vector
#'
#' @importFrom bkmisc check_numeric
#'
#' @export
#'
#' @examples
#' #----------------------------------------------------------------------------
#' # standardize() examples
#' #----------------------------------------------------------------------------
#' library(bkstat)
#'
#' x <- rnorm(100, mean = 30, sd = 20)
#' standardize(x)
#'
standardize <- function(
  x,
  center = function(x) mean(x, na.rm = TRUE),
  scale = function(x) sd(x, na.rm = TRUE)
) {
  #-----------------------------------------------------------------------------
  # Check arguments
  #-----------------------------------------------------------------------------
  check_numeric(x)
  if(is.function(center)) {
    center <- center(x)
  }
  if(is.function(scale)) {
    scale <- scale(x)
  }
  check_numeric(center)
  check_numeric(scale)
  stopifnot(length(center) == 1)
  stopifnot(length(scale) == 1)

  #-----------------------------------------------------------------------------
  # Return
  #-----------------------------------------------------------------------------
  (x - center) / scale
}
