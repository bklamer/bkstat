library(testit)

#-------------------------------------------------------------------------------
# Data
#-------------------------------------------------------------------------------
set.seed(20231201)
df1 <- MASS::mvrnorm(n = 30, mu = c(2, 3), Sigma = matrix(c(2, 0, 0, 2), 2L, 2L))
df2 <- MASS::mvrnorm(n = 30, mu = c(2, 3), Sigma = matrix(c(2, 0.5, 0.5, 2), 2L, 2L))

#-------------------------------------------------------------------------------
# Tests
#-------------------------------------------------------------------------------
assert(
  "t_test_fast() has same result as t.test().",
  # Independent two sample
  all.equal(
    t_test_fast(
      x = df1[,1],
      y = df1[,2],
      alternative = "two.sided",
      paired = FALSE,
      var.equal = FALSE
    ),
    t.test(
      x = df1[,1],
      y = df1[,2],
      alternative = "two.sided",
      paired = FALSE,
      var.equal = FALSE
    )$p.value
  ),
  # Dependent two sample
  all.equal(
    t_test_fast(
      x = df2[,1],
      y = df2[,2],
      alternative = "two.sided",
      paired = TRUE
    ),
    t.test(
      x = df2[,1],
      y = df2[,2],
      alternative = "two.sided",
      paired = TRUE
    )$p.value
  ),
  # Dependent one sample
  all.equal(
    t_test_fast(
      x = df2[,2] - df2[,1],
      alternative = "two.sided"
    ),
    t.test(
      x = df2[,2] - df2[,1],
      alternative = "two.sided"
    )$p.value
  )
)

assert(
  "t_test_fast() paired test has same result both ways.",
  # Dependent two sample vs. Dependent one sample
  all.equal(
    t_test_fast(
      x = df2[,1],
      y = df2[,2],
      alternative = "two.sided",
      paired = TRUE
    ),
    t_test_fast(
      x = df2[,2] - df2[,1],
      alternative = "two.sided"
    )
  )
)
