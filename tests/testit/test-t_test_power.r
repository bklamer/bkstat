library(testit)

#-------------------------------------------------------------------------------
# Data
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
# Tests
#-------------------------------------------------------------------------------
assert(
  "t_test_power() independent two sample has verified result.",
  # Independent two sample
  all.equal(
    t_test_power(
      n1 = 30,
      n2 = 30,
      fc_null = 1,
      fc_true = 1.3,
      cv1 = 0.35,
      cv2 = 0.35,
      iters = 50000
    ),
    0.8363,
    tolerance = 0.007
  ),
  # Independent two sample (inverted fc)
  all.equal(
    t_test_power(
      n1 = 30,
      n2 = 30,
      fc_null = 1,
      fc_true = 1/1.3,
      cv1 = 0.35,
      cv2 = 0.35,
      iters = 50000
    ),
    0.8363,
    tolerance = 0.007
  ),
  # Independent two sample (different sample sizes)
  all.equal(
    t_test_power(
      n1 = 30,
      n2 = 100,
      fc_null = 1,
      fc_true = 1.3,
      cv1 = 0.35,
      cv2 = 0.35,
      iters = 50000
    ),
    0.9573,
    tolerance = 0.007
  )
)

assert(
  "t_test_power() paired two sample has verified result.",
  # Independent two sample (different CV)
  all.equal(
    t_test_power(
      n1 = 30,
      n2 = 30,
      fc_null = 1,
      fc_true = 1.1,
      cv1 = 0.3,
      cv2 = 0.5,
      cor = 0.345,
      iters = 50000
    ),
    0.19762,
    tolerance = 0.02
  ),
  # Paired two sample (Same CV)
  all.equal(
    t_test_power(
      n1 = 30,
      n2 = 30,
      fc_null = 1,
      fc_true = 1.1,
      cv1 = 0.3,
      cv2 = 0.3,
      cor = 0.345,
      iters = 50000
    ),
    0.32798,
    tolerance = 0.015
  ),
  # Paired two sample (Different CV)
  all.equal(
    t_test_power(
      n1 = 30,
      n2 = 30,
      fc_null = 1,
      fc_true = 1.3,
      cv1 = 0.7,
      cv2 = 0.4,
      cor = 0.765,
      iters = 50000
    ),
    0.93598,
    tolerance = 0.007
  )
)

assert(
  "t_test_power() paired one sample has expected result.",
  # Paired one sample
  all.equal(
    t_test_power(
      n1 = 30,
      fc_null = 1,
      fc_true = 1.3,
      cv1 = 0.35,
      iters = 50000
    ),
    0.983,
    tolerance = 0.007
  )
)
